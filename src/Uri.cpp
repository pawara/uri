/**
 * @file Uri.cpp
 *
 * This module contains the implementation of the Uri::Uri class.
 *
 */
//#include <inttypes.h> // to SCNu16
//#include <stdio.h> // C library header to convert string to int
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <Uri/Uri.hpp>
#include <functional>
#include <memory>
#include <sstream>
#include "NormalizeCaseInsensitiveString.hpp"
#include "CharacterSet.hpp"
#include "PercentEncodeCharacterDecoder.hpp"

//using namespace Uri;
namespace
{
    /**
    ADDITIONAL
    ===========
        Using { } - means using initializer list
        Uri::CharacterSet{'0', '9'} - do not call  Uri::CharacterSet(char first, char last) constructor
        as this passes initalizer list
     */

     /**
      * This is the character set containing just the alphabetic characters
      * from the ASCII character set.
      */
    const Uri::CharacterSet ALPHA
    {
        // this call move constructor
        Uri::CharacterSet('a', 'z'),
        Uri::CharacterSet('A', 'Z')
    };

    /**
     * This is the character set containing just numbers.
     */
     // this don't need initializer list, need fisrt last char constructor to call
    const Uri::CharacterSet DIGIT('0', '9');

    /**
     * This is the character set containing just the characters allowed
     * in a hexadecimal digit.
     */
    const Uri::CharacterSet HEXDIG{
        Uri::CharacterSet('0', '9'),
        Uri::CharacterSet('A', 'F'),
        Uri::CharacterSet('a', 'f')
    };

    /**
     * This is the character set corresponds to the "unreserved" syntax
     * specified in RFC 3986 (https://tools.ietf.org/html/rfc3986).
     */

     /**
     ADDITIONAL
     ===========
        // One use case of a copy constructor is
        // When the object is passed to a function as a non - reference parameter
        // as i nhere
        // so created the copy constructor for CharacterSet
      */

    const Uri::CharacterSet UNRESERVED{
        ALPHA,
        DIGIT,
        '-', '.', '_', '~'
    };

    /**
     * This is the character set corresponds to the "sub-delims" syntax
     * specified in RFC 3986 (https://tools.ietf.org/html/rfc3986).
     */
    const Uri::CharacterSet SUB_DELIMS{
        '!', '$', '&', '\'', '(', ')',
        '*', '+', ',', ';', '='
    };

    /**
     * This is the character set corresponds to the second part
     * of the "scheme" syntax
     * specified in RFC 3986 (https://tools.ietf.org/html/rfc3986).
     */
    const Uri::CharacterSet SCHEME_NOT_FIRST{
        ALPHA,
        DIGIT,
        '+', '-', '.',
    };

    /**
     * This is the character set corresponds to the "pchar" syntax
     * specified in RFC 3986 (https://tools.ietf.org/html/rfc3986),
     * leaving out "pct-encoded".
     */
    const Uri::CharacterSet PCHAR_NOT_PCT_ENCODED{
        UNRESERVED,
        SUB_DELIMS,
        ':', '@'
    };

    /**
     * This is the character set corresponds to the "query" syntax
     * and the "fragment" syntax
     * specified in RFC 3986 (https://tools.ietf.org/html/rfc3986),
     * leaving out "pct-encoded".
     */
    const Uri::CharacterSet QUERY_OR_FRAGMENT_NOT_PCT_ENCODED{
        PCHAR_NOT_PCT_ENCODED,
        '/', '?'
    };

    /**
     * This is the character set corresponds to the "userinfo" syntax
     * specified in RFC 3986 (https://tools.ietf.org/html/rfc3986),
     * leaving out "pct-encoded".
     */
    const Uri::CharacterSet USER_INFO_NOT_PCT_ENCODED{
        UNRESERVED,
        SUB_DELIMS,
        ':',
    };

    /**
     * This is the character set corresponds to the "reg-name" syntax
     * specified in RFC 3986 (https://tools.ietf.org/html/rfc3986),
     * leaving out "pct-encoded".
     */
    const Uri::CharacterSet REG_NAME_NOT_PCT_ENCODED{
        UNRESERVED,
        SUB_DELIMS
    };

    /**
     * This is the character set corresponds to the last part of
     * the "IPvFuture" syntax
     * specified in RFC 3986 (https://tools.ietf.org/html/rfc3986).
     */
    const Uri::CharacterSet IPV_FUTURE_LAST_PART{
        UNRESERVED,
        SUB_DELIMS,
        ':'
    };


    /**
     * This function parses the given string as an unsigned 16-bit
     * integer, detecting invalid characters, overflow, etc.
     *
     * @param[in] numberString
     *     This is the string containing the number to parse.
     *
     * @param[out] number
     *     This is where to store the number parsed.
     *
     * @return
     *     An indication of whether or not the number was parsed
     *     successfully is returned.
     */

    bool ParseUint16(const std::string& numberString, uint16_t& number)
    {
        //To convert a string to int can use C++ or C. Lets do in C way. stdio.h

        //ADDITIONAL
        //    C way to get the number from a string

        //if (
        //    sscanf
        //    (
        //    rest.substr(portDelimeter + 1, authorityEnd - portDelimeter - 1).c_str(),
        //    "%" SCNu16, // format - scan unsigned 16 (this is a macro value defined as "hu". here gives from the macro ithout hardcoding) (this SCNu16 tells unsigned 16 bit)
        //    // similar to passing "%hu" ("%" SCNu16 = "%hu")
        //    &impl_->port
        //    ) != 1
        //)
        //{
        //    return false;
        //}

        // Common way to do in C++

        uint32_t numberIn32Bits = 0;

        for (auto c : numberString)
        {
            if (c < '0' || c>'9')
            {
                return false;
            }
            numberIn32Bits = numberIn32Bits * 10;
            numberIn32Bits += (uint16_t)(c - '0');

            // In any case 16th bit of newPort get set, then it overflows
            // & - bitise operator AND
            // << - bitise LEFT SHIFT operator
            // ~ - bitise negation operator. invert all bits (1's for 16th to 32nd character. 0's for 0 to 16th index)
            // not equal zero means have not zero elements between 16th-32nd indexes of newPort
            if ((numberIn32Bits & ~((1 << 16) - 1)) != 0)
            {
                return false;
            }
        }
        number = (uint16_t)numberIn32Bits;
        return true;
    }


    /*
    ADDITIONAL
    ============
        // takes string which need to check for invalid characters as the first argument
        // second argument is the function, how to check invalid characters.

        std::function is a template

            std::function< return_type_of_the_func (func_arguments) > func_name
    */

    /**
     * This function takes a given "stillPassing" strategy
     * and invokes it on the sequence of characters in the given
     * string, to check if the string passes or not.
     *
     * @param[in] candidate
     *     This is the string to test.
     *
     * @param[in] stillPassing
     *     This is the strategy to invoke in order to test the string.
     *
     * @return
     *     An indication of whether or not the given candidate string
     *     passes the test is returned.
     */

    bool FailsMatch(const std::string& candidate, std::function<bool(char, bool)> stillPassing)
    {
        for (const auto c : candidate)
        {
            if (!stillPassing(c, false))
            {
                return true;
            }

        }
        return !stillPassing(' ', true);
    }




    /**
        * This function returns a strategy function that
        * may be used with the FailsMatch function to test a scheme
        * to make sure it is legal according to the standard.
        *
        * @return
        *      A strategy function that may be used with the
        *      FailsMatch function to test a scheme to make sure
        *      it is legal according to the standard is returned.
        */

        /*
            ADDITIONAL
            ============
            As the below function (lamda) capturing a boolean (isFirstCharacter) we need more than a free function when refactoring
            So need an object
            Unlike in the school, u don't need to do a function class all the time
            It need only when need to seperate public from private stuff
            This kinda thing all internal. So lets make a struct and refactor

        */

        /*
            ADDITIONAL
            ============
            // lambda functions
            [captures(these take from the outside)](arguments)->return_type
            {

            };

        */

        /*
        ADDITIONAL
        ============
        C, C++ are traditionally called imparative programming languages.
            Like: DO (a), DO (b), DO (c)
            Beginning to end steps of instructions
            This is more like functional programming where functions are variables and objects you pass around just like numbers and strings
            Can make a function which build and return function


            REASON WHY USED HERE:
                Function passing to fails match need some state. We used the trick called closure
                Making the state inside the LegalScheemCheckStrategy() local variable and capturing it by lambda function

                Concept is - To capture variable in function object and use it internally
        */
    std::function<bool(char, bool)> LegalScheemCheckStrategy()
    {
        /*
        ADDITIONAL
        ============
        // this isFirstCharacter is not safe because it creates in the stack and use the same within the function.
        // When LegalScheemCheckStrategy finish value will be gone

        //bool isFirstCharacter = true;


        // Lets use memory library to solve
        */
        /*std::shared_ptr<bool> isFirstCharacter;
        *isFirstCharacter = true;*/

        /*
        ADDITIONAL
        ============
        std::shared_ptr is a smart pointer that retains shared ownership of an object through a pointer.
        Several shared_ptr objects may own the same object.The object is destroyed and its memory deallocated when either of the following happens :

            * the last remaining shared_ptr owning the object is destroyed;
            * the last remaining shared_ptr owning the object is assigned another pointer via operator= or reset().
        */
        auto isFirstCharacter = std::make_shared<bool>(true);

        return [isFirstCharacter](char c, bool end)-> bool
        {
            if (end)
            {
                return !*isFirstCharacter;
            }
            else
            {
                bool check;
                if (*isFirstCharacter)
                {
                    check = ALPHA.Contains(c);
                }
                else
                {
                    check = SCHEME_NOT_FIRST.Contains(c);
                }
                *isFirstCharacter = false;
                return check;
            }

        };
    }

    /**
     * This function checks to make sure the given string
     * is a valid rendering of an octet as a decimal number.
     *
     * @param[in] octetString
     *     This is the octet string to validate.
     *
     * @return
     *     An indication of whether or not the given astring
     *     is a valid rendering of an octet as a
     *     decimal number is returned.
     */
    bool ValidateOctet(const std::string& octetString) {
        int octet = 0;
        for (auto c : octetString) {
            if (DIGIT.Contains(c)) {
                octet *= 10;
                octet += (int)(c - '0');
            }
            else {
                return false;
            }
        }
        return (octet <= 255);
    }

    /**
     * This function checks to make sure the given address
     * is a valid IPv6 address according to the rules in
     * RFC 3986 (https://tools.ietf.org/html/rfc3986).
     *
     * @param[in] address
     *     This is the IPv6 address to validate.
     *
     * @return
     *     An indication of whether or not the given address
     *     is a valid IPv6 address is returned.
     */
    bool ValidateIpv4Adress(const std::string& address) {
        size_t numGroups = 0;
        size_t state = 0;
        std::string octetBuffer;
        for (auto c : address) {
            switch (state) {
            case 0: { // not in an octet yet
                if (DIGIT.Contains(c)) {
                    octetBuffer.push_back(c);
                    state = 1;
                }
                else {
                    return false;
                }
            } break;

            case 1: { // expect a digit or dot
                if (c == '.') {
                    if (numGroups++ >= 4) {
                        return false;
                    }
                    if (!ValidateOctet(octetBuffer)) {
                        return false;
                    }
                    octetBuffer.clear();
                    state = 0;
                }
                else if (DIGIT.Contains(c)) {
                    octetBuffer.push_back(c);
                }
                else {
                    return false;
                }
            } break;
            }
        }
        if (!octetBuffer.empty()) {
            ++numGroups;
            if (!ValidateOctet(octetBuffer)) {
                return false;
            }
        }
        return (numGroups == 4);
    }

    /**
     * This function checks to make sure the given address
     * is a valid IPv6 address according to the rules in
     * RFC 3986 (https://tools.ietf.org/html/rfc3986).
     *
     * @param[in] address
     *     This is the IPv6 address to validate.
     *
     * @return
     *     An indication of whether or not the given address
     *     is a valid IPv6 address is returned.
     */
    bool ValidateIpv6Address(const std::string& address) {
        size_t numGroups = 0;
        size_t numDigits = 0;
        bool doubleColonEncountered = false;
        size_t state = 0;
        size_t potentialIpv4AddressStart = 0;
        size_t position = 0;
        bool ipv4AddressEncountered = false;
        for (auto c : address) {
            switch (state) {
            case 0: { // not in a group yet
                if (c == ':') {
                    state = 1;
                }
                else if (DIGIT.Contains(c)) {
                    potentialIpv4AddressStart = position;
                    ++numDigits;
                    state = 4;
                }
                else if (HEXDIG.Contains(c)) {
                    ++numDigits = 1;
                    state = 3;
                }
                else {
                    return false;
                }
            } break;

            case 1: { // not in a group yet, encountered one colon
                if (c == ':') {
                    if (doubleColonEncountered) {
                        return false;
                    }
                    else {
                        doubleColonEncountered = true;
                        state = 2;
                    }
                }
                else {
                    return false;
                }
            } break;

            case 2: { // expect a hex digit
                if (DIGIT.Contains(c)) {
                    potentialIpv4AddressStart = position;
                    if (++numDigits > 4) {
                        return false;
                    }
                    state = 4;
                }
                else if (HEXDIG.Contains(c)) {
                    if (++numDigits > 4) {
                        return false;
                    }
                    state = 3;
                }
                else {
                    return false;
                }
            } break;

            case 3: { // expect either a hex digit or colon
                if (c == ':') {
                    numDigits = 0;
                    ++numGroups;
                    state = 2;
                }
                else if (HEXDIG.Contains(c)) {
                    if (++numDigits > 4) {
                        return false;
                    }
                }
                else {
                    return false;
                }
            } break;

            case 4: { // expect either a hex digit, dot, or colon
                if (c == ':') {
                    numDigits = 0;
                    ++numGroups;
                }
                else if (c == '.') {
                    ipv4AddressEncountered = true;
                    break;
                }
                else if (DIGIT.Contains(c)) {
                    if (++numDigits > 4) {
                        return false;
                    }
                }
                else if (HEXDIG.Contains(c)) {
                    if (++numDigits > 4) {
                        return false;
                    }
                    state = 3;
                }
                else {
                    return false;
                }
            } break;
            }
            if (ipv4AddressEncountered) {
                break;
            }
            ++position;
        }
        if (state == 4) {
            // count trailing group
            ++numGroups;
        }
        if (
            (position == address.length())
            && (state == 1)
            ) { // trailing single colon
            return false;
        }
        if (ipv4AddressEncountered) {
            if (!ValidateIpv4Adress(address.substr(potentialIpv4AddressStart))) {
                return false;
            }
            numGroups += 2;
        }
        if (doubleColonEncountered) {
            // A double colon matches one or more groups (of 0).
            return (numGroups <= 7);
        }
        else {
            return (numGroups == 8);
        }
    }

    ///**
    // * This function checks to make sure the given address
    // * is a valid IPv6 address according to the rules in
    // * RFC 3986 (https://tools.ietf.org/html/rfc3986).
    // *
    // * @param[in] address
    // *     This is the IPv6 address to validate.
    // *
    // * @return
    // *     An indication of whether or not the given address
    // *     is a valid IPv6 address is returned.
    // */
    //bool ValidateIPv6Address(const std::string & address)
    //{
    //    /*
    //    ADDITIONAL - State machine applicable problems
    //    ============

    //    There can be different approaches.
    //    One way to handle this kind of problem is to develop a state machine

    //    */

    //    /*
    //    ADDITIONAL - State machine approach
    //    ============

    //    Probably need a starting state for the state machine.

    //    Then there would be several posibilities from the starting state based on the first input (first character for here kind example.)
    //    Based on the first input change the from initial state to the next state. Repeat until the last input element. 

    //    */

    //    size_t numGroups = 0;
    //    bool doubleColonEncountered = false;
    //    size_t numOfDigits = 0;
    //    size_t state = 0;
    //    std::string octetBuffer;

    //    for (auto c : address)
    //    {
    //        switch (state)
    //        {
    //            case 0: //not in a group yet
    //            {
    //                if (c == ':')
    //                {
    //                    state = 1;
    //                }
    //                else if (DIGIT.Contains(c))
    //                {
    //                    ++numOfDigits;
    //                    state = 4;

    //                }
    //                else if (HEXDIG.Contains(c))
    //                {
    //                    //TODO state change
    //                    numOfDigits += 1;
    //                    if (numOfDigits > 4)
    //                    {
    //                        return false;
    //                    }
    //                    state = 3;
    //                }
    //                else
    //                {
    //                    return false;
    //                }
    //            }break;

    //            case 1: // not in a group yet, encountered a colon
    //            {
    //                if (c == ':')
    //                {
    //                    if (doubleColonEncountered)
    //                    {
    //                        return false;
    //                    }
    //                    else
    //                    {
    //                        doubleColonEncountered = true;
    //                        state = 2;
    //                    }
    //                }
    //                else
    //                {
    //                    return false;
    //                }
    //            }break;

    //            case 2:// expect a hex digit
    //            {
    //                if (HEXDIG.Contains(c))
    //                {
    //                    numOfDigits += 1;
    //                    if (numOfDigits > 4)
    //                    {
    //                        return false;
    //                    }
    //                    state = 3;
    //                }
    //                else
    //                {
    //                    return false;
    //                }
    //            }break;

    //            case 3:// expect either a hex digit or colon
    //            {
    //                if (HEXDIG.Contains(c))
    //                {
    //                    numOfDigits += 1;
    //                    if (numOfDigits > 4)
    //                    {
    //                        return false;
    //                    }
    //                }
    //                else if(c == ':')
    //                {
    //                    ++numGroups;

    //                    numOfDigits = 0;
    //                    state = 2;
    //                }
    //                
    //            }break;

    //            case 4:// expect either a hex digit, dot, or colon
    //            {
    //                if (HEXDIG.Contains(c))
    //                {
    //                    numOfDigits += 1;
    //                    if (numOfDigits > 4)
    //                    {
    //                        return false;
    //                    }
    //                }
    //                else if (c == ':')
    //                {
    //                    ++numGroups;

    //                    numOfDigits = 0;
    //                    state = 1;
    //                }

    //            }break;
    //        }
    //    }
    //    if (state == 1) // trailing single colon
    //    {
    //        return false;
    //    }

    //    if (doubleColonEncountered)
    //    {
    //        // A double colon matches one or more groups(of 0).
    //        return (numGroups <= 7);
    //    }
    //    else
    //    {
    //        return (numGroups == 8);
    //    }

    //    return true;
    //}
    /*
    ADDITIONAL
    ============
    Importance of code refactoring -
        remove duplication
        easier to read
    */

    /**
     * This method checks and decodes the given URI element.
     * What we are calling a "URI element" is any part of the URI
     * which is a sequence of characters that:
     * - may be percent-encoded
     * - if not percent-encoded, are in a restricted set of characters
     *
     * @param[in,out] element
     *     On input, this is the element to check and decode.
     *     On output, this is the decoded element.
     *
     * @param[in] allowedCharacters
     *     This is the set of characters that do not need to
     *     be percent-encoded.
     *
     * @return
     *     An indication of whether or not the element
     *     passed all checks and was decoded successfully is returned.
     */
    bool DecodeElement(std::string& element, const Uri::CharacterSet& allowedCharacters)
    {
        const auto originalSegment = std::move(element);

        // clear is safe to do after you moved it into somewhere else
        element.clear();
        bool decodingPec = false;
        Uri::PercentEncodeCharacterDecoder pecDecoder;
        for (const auto c : originalSegment)
        {
            if (decodingPec)
            {
                if (!pecDecoder.NextEncodedCharacter(c))
                {
                    return false;
                }
                if (pecDecoder.Done())
                {
                    decodingPec = false;
                    element.push_back((char)pecDecoder.GetDecodedCharacter());
                }
            }
            else if (c == '%')
            {
                decodingPec = true;
                pecDecoder = Uri::PercentEncodeCharacterDecoder();
            }
            else
            {
                if (allowedCharacters.Contains(c))
                {
                    element.push_back(c);
                }
                else
                {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * This method checks and decodes the given path segment.
     *
     * @param[in,out] segment
     *     On input, this is the path segment to check and decode.
     *     On output, this is the decoded path segment.
     *
     * @return
     *     An indication of whether or not the path segment
     *     passed all checks and was decoded successfully is returned.
     */
    bool DecodePathSegment(std::string& queryOrFragment)
    {
        return DecodeElement(queryOrFragment, PCHAR_NOT_PCT_ENCODED);
    }

    /**
     * This method checks and decodes the given query or fragment.
     *
     * @param[in,out] queryOrFragment
     *     On input, this is the query or fragment to check and decode.
     *     On output, this is the decoded query or fragment.
     *
     * @return
     *     An indication of whether or not the query or fragment
     *     passed all checks and was decoded successfully is returned.
     */
    bool DecodeQueryOrFragment(std::string& queryOrFragment)
    {
        return DecodeElement(queryOrFragment, QUERY_OR_FRAGMENT_NOT_PCT_ENCODED);
    }

    /**
     * This method checks and decodes the given user info.
     *
     * @param[in,out] segment
     *     On input, this is the path segment to check and decode.
     *     On output, this is the decoded path segment.
     *
     * @return
     *     An indication of whether or not the path segment
     *     passed all checks and was decoded successfully is returned.
     */
    bool DecodeUserInfo(std::string& queryOrFragment)
    {
        return DecodeElement(queryOrFragment, USER_INFO_NOT_PCT_ENCODED);
    }
}
namespace Uri {
    /**
     * This contains the private properties of a Uri instance.
     */
    struct Uri::Impl
    {
        // Reverted the custom path delimeter as it is constant '/'

        // to get one method to the other, we use state
        // this is the "scheme" element of the URI

        std::string scheme;

        /**
         * This is the "UserInfo" element of the URI.
         */
        std::string userInfo;

        // to get one method to the other, we use state
        // this is the "host" element of the URI

        std::string host;


        /**
         * This flag indicates whether or not the
         * URI includes a port number.
         */

         /*
         ADDITIONAL
         ============

         Strings, Vectors have a default value.
         Trivial types don't have a default value
         It is good to have a default value

         */

        bool hasPort = false;

        /**
         * This is the port number element of the URI.
         */
        uint16_t port = 0;



        // to get one method to the other, we use state
        // this is the "path" element of the URI
        /* *
            The "path" element of the URI is returned
            as a sequence of steps.
        */
        std::vector<std::string> path;


        /**
         * This is the "query" element of the URI,
         * if it has one.
         */
        std::string query;



        // IsRelative` -- flag indicating whether or not the reference is `relative reference` (as opposed to a `URI`)
        // Relative Path and Relative Reference are 2 different concepts
        // Relative Reference just starts with the relative part. Doesn't have a scheme

        bool isRelativeReference = false;

        /**
         * This is the "fragment" element of the URI,
         * if it has one.
         */
        std::string fragment;



        // Methods



        /**
         * This method builds the internal path element sequence
         * by parsing it from the given path string.
         *
         * @param[in] pathString
         *     This is the string containing the whole path of the URI.
         *
         * @return
         *     An indication if the path was parsed correctly or not
         *     is returned.
         */

         //bool ParsePath(std::string& pathString)
        bool ParsePath(std::string pathString) // get a copy and change it locally
        {
            // Parse the path

            /*Expected outcomes
                "" -> []
                "/" -> [""]
                "/foo" -> ["foo"]
                "foo/" -> ["foo", ""]
            */
            //auto pathString = hostPortAndPathString;

            this->path.clear();



            if (pathString == "/")
            {
                // Special case of that is empty but needs a single empty-string element to indicate that it is absolute
                this->path.push_back("");
                pathString.clear();
            }
            else if (!pathString.empty())
            {
                for (;;)
                {
                    /*
                    ADDITIONAL
                    ============
                    auto - compiler detects type by looking at the rest
                    If ambigous (two return types for the same function) compiler can't detect
                    auto - This is called type influence (Compiler detects type for you)
                    */

                    auto pathDelimeter = pathString.find('/');

                    if (pathDelimeter == std::string::npos)
                    {
                        this->path.push_back(pathString);
                        pathString.clear();
                        break;
                    }
                    else
                    {
                        this->path.emplace_back(pathString.begin(), pathString.begin() + pathDelimeter);

                        pathString = pathString.substr(pathDelimeter + 1);
                    }

                }
            }
            for (auto& segment : path)
            {
                if (!DecodePathSegment(segment))
                {
                    return false;
                }
            }
            return true;
        }

        /*
        ADDITIONAL
        ===========
        * C++ has two kinds of enum:

            enum classes
            ------------
                enum class Color { red, green, blue }; // enum class

            Plain enums
            -----------
                enum Animal { dog, cat, bird, human }; // plain enum 

            Difference
            ----------
            enum classes -
                - enumerator names are local to the enum
                - and their values do not implicitly convert to other types (like another enum or int)

            Plain enums -
                - where enumerator names are in the same scope as the enum
                - and their values implicitly convert to integers and other types
        */

        /**
         * These are the various states for the state machine implemented
         * below to correctly split up and validate the URI substring
         * containing the host and potentially a port number as well.
         */
        enum class HostParsingState {
            FIRST_CHARACTER,
            NOT_IP_LITERAL,
            PERCENT_ENCODED_CHARACTER,
            IP_LITERAL,
            IPV6_ADDRESS,
            IPV_FUTURE_NUMBER,
            IPV_FUTURE_BODY,
            GARBAGE_CHECK,
            PORT,
        };

        /*
        ADDITIONAL - constexpr
        ===========

        * constexpr indicates that the value, or return value, is constant and,
        where possible, is computed at compile time.
        * A constexpr integral value can be used wherever a const integer is required.

        */
        
        /**
         * This method parses the elements that make up the authority
         * composite part of the URI,  by parsing it from the given string.
         *
         * @param[in] authorityString
         *     This is the string containing the whole authority part
         *     of the URI.
         *
         * @return
         *     An indication if the path was parsed correctly or not
         *     is returned.
         */
        bool ParseAuthority(const std::string authorityString)
        {
            /*
            ADDITIONAL
            ===========
            * For state machines it is good to name states with a semantic meaningful name,
            rather than using raw numbers
            */

            // Next, check if there is a UserInfo, and if so, extract it.
            const auto userInfoDelimiter = authorityString.find('@');

            std::string hostAndPortString;
            userInfo.clear();
            if (userInfoDelimiter == std::string::npos) {

                hostAndPortString = authorityString;
            }
            else
            {
                userInfo = authorityString.substr(0, userInfoDelimiter);

                if (!DecodeUserInfo(userInfo))
                {
                    return false;
                }

                hostAndPortString = authorityString.substr(userInfoDelimiter + 1);
            }

            // Next, parsing host and port from authority and path.

            //const auto portDelimeter = hostAndPortString.find(':'); // find ':' if have scheme

            //std::string hostEncoded;

            //if (portDelimeter == std::string::npos) // npos likes compare with -1
            //{
            //    //host = hostAndPortString.substr(0, authorityEnd); // substr(starting point, length)
            //    hostEncoded = hostAndPortString; // substr(starting point, length)
            //    hasPort = false;
            //}
            //else
            //{
            //    hostEncoded = hostAndPortString.substr(0, portDelimeter); // substr(starting point, length)

            //    //const auto portString = hostAndPortString.substr(portDelimeter + 1, authorityEnd - portDelimeter - 1);
            //    const auto portString = hostAndPortString.substr(portDelimeter + 1);
            //    if (!ParseUint16(portString, port))
            //    {
            //        return false;
            //    }

            //    hasPort = true;
            //}


            std::string portString;
            HostParsingState hostParsingState = HostParsingState::FIRST_CHARACTER;
            int decodedCharacter = 0;
            host.clear();
            PercentEncodeCharacterDecoder pecDecoder;
            bool hostIsRegName = false;
            for (const auto c : hostAndPortString)
            {
                switch (hostParsingState)
                {
                case HostParsingState::FIRST_CHARACTER: // first character
                {
                    if (c == '[')
                    {
                        //host.push_back(c);
                        hostParsingState = HostParsingState::IP_LITERAL;
                        break;
                    }
                    else
                    {
                        hostParsingState = HostParsingState::NOT_IP_LITERAL;
                        hostIsRegName = true;
                    }
                }

                case HostParsingState::NOT_IP_LITERAL: // reg-name or IPv4Address
                {
                    if (c == '%')
                    {
                        pecDecoder = PercentEncodeCharacterDecoder();
                        hostParsingState = HostParsingState::PERCENT_ENCODED_CHARACTER;
                    }
                    else if (c == ':')
                    {
                        hostParsingState = HostParsingState::PORT;
                    }
                    else
                    {
                        if (REG_NAME_NOT_PCT_ENCODED.Contains(c))
                        {
                            host.push_back(c);
                        }
                        else
                        {
                            return false;
                        }

                    }
                }break;
                case HostParsingState::PERCENT_ENCODED_CHARACTER: // % ..
                {
                    if (!pecDecoder.NextEncodedCharacter(c))
                    {
                        return false;
                    }
                    if (pecDecoder.Done())
                    {
                        hostParsingState = HostParsingState::NOT_IP_LITERAL;
                        host.push_back((char)pecDecoder.GetDecodedCharacter());
                    }
                }break;
                case HostParsingState::IP_LITERAL: // IP-literal
                {
                    if (c == 'v')
                    {
                        host.push_back(c);
                        hostParsingState = HostParsingState::IPV_FUTURE_NUMBER;
                        break;
                    }
                    else
                    {
                        hostParsingState = HostParsingState::IPV6_ADDRESS;
                    }
                }

                case HostParsingState::IPV6_ADDRESS: // IPv6Address:
                {
                    // TODO: research this offline first before attempting to code it
                    //host.push_back(c);
                    if (c == ']')
                    {
                        /*
                        ADDITIONAL - IPv6 validations
                        ===========

                        * It is requires to validate IPv6 address whether it has the correct number of groups
                        * Possibilities:
                            * 8 groups each separated by a colon
                            * :: separated two groups with maximum to 7 total groups
                            * Cannot be more than one :: separated pair of groups
                        */
                        /*
                        ADDITIONAL - free functions
                        ===========

                        * We can make a free function by parsing the arguments from outside.

                        * As a one way of practise, we can make a free function first,
                        * in the anonymous namespace in the same module and then,
                        * as need to:
                            promote them to the module
                            promote them to the library
                        that kind of progression

                        */

                        if (!ValidateIpv6Address(host))
                        {
                            return false;
                        }
                        hostParsingState = HostParsingState::GARBAGE_CHECK;
                    }
                    else
                    {
                        host.push_back(c);
                    }
                }break;

                case HostParsingState::IPV_FUTURE_NUMBER: // IPvFuture: v ...
                {
                    if (c == '.')
                    {
                        hostParsingState = HostParsingState::IPV_FUTURE_BODY;
                    }
                    else if (!HEXDIG.Contains(c))
                    {
                        return false;
                    }
                    host.push_back(c);
                }break;
                case HostParsingState::IPV_FUTURE_BODY: // IPvFuture: v 1*HEXDIG . ...
                {
                    
                    if (c == ']')
                    {
                        hostParsingState = HostParsingState::GARBAGE_CHECK;
                    }
                    else if (!IPV_FUTURE_LAST_PART.Contains(c))
                    {
                        return false;
                    }
                    else
                    {
                        host.push_back(c);
                    }
                }break;

                case HostParsingState::GARBAGE_CHECK:
                { // illegal to have anything else, unless it's a colon,
                        // in which case it's a port delimiter
                    if (c == ':') {
                        hostParsingState = HostParsingState::PORT;
                    }
                    else
                    {
                        return false;
                    }
                }break;

                case HostParsingState::PORT: { // port
                    portString.push_back(c);
                } break;

                }
            }

            if (
                (hostParsingState != HostParsingState::FIRST_CHARACTER)
                && (hostParsingState != HostParsingState::NOT_IP_LITERAL)
                && (hostParsingState != HostParsingState::GARBAGE_CHECK)
                && (hostParsingState != HostParsingState::PORT)
                ) {
                // truncated or ended early
                return false;
            }
            if (hostIsRegName)
            {
                host = NormalizeCaseInsensitiveString(host);
            }
            if (portString.empty())
            {
                hasPort = false;
            }
            else
            {
                if (!ParseUint16(portString, port)) {
                    return false;
                }
                hasPort = true;
            }
            return true;
        }

        /**
         * This method takes an unparsed URI string and separates out
         * the scheme (if any) and parses it, returning the remainder
         * of the unparsed URI string.
         *
         * @param[in] authorityAndPathString
         *     This is the the part of an unparsed URI consisting
         *     of the authority (if any) followed by the path.
         *
         * @param[out] pathString
         *     This is where to store the the path
         *     part of the input string.
         *
         * @return
         *     An indication of whether or not the given input string
         *     was successfully parsed is returned.
         */
        bool ParseScheme(
                            const std::string& uriString, //input 
                            std::string& rest //output
        )
        {
            this->scheme.clear();
            // Limit our search so we don't scan into the authority
            // or path elements, because these may have the colon
            // character as well, which we might misinterpret
            // as the scheme delimiter.
            auto authorityOrPathDelimiterStart = uriString.find('/');
            if (authorityOrPathDelimiterStart == std::string::npos)
            {
                authorityOrPathDelimiterStart = uriString.length();
            }

            /*
            ADDITIONAL
            ===========

            use const whenever possible, and only omit it if necessary
            const may enable the compiler to optimize
            understand how your code is intended to be used (and the compiler will catch possible misuse)
            */

            const auto schemeEnd = uriString.substr(0, authorityOrPathDelimiterStart).find(':');
            

            /*
            ADDITIONAL
            ===========

            static const size_t npos = -1;

            npos is a static member constant value with the greatest possible value for an element of type size_t.
            As a return value, it is usually used to indicate no matches.

            */
            if (schemeEnd == std::string::npos)
            {
                scheme.clear();
                rest = uriString;
            }
            else
            {
                scheme = uriString.substr(0, schemeEnd);

                bool isFirstCharacter = true;

                if (
                    FailsMatch
                    (
                        scheme,
                        LegalScheemCheckStrategy()
                    )
                    )
                {
                    return false;
                }
                scheme = NormalizeCaseInsensitiveString(scheme);
                rest = uriString.substr(schemeEnd + 1);
            }
            return true;
        }

        /**
         * This method takes the part of an unparsed URI consisting
         * of the authority (if any) followed by the path, and divides
         * it into the authority and path parts, storing any authority
         * information in the internal state, and returning the path
         * part of the input string.
         *
         * @param[in] authorityAndPathString
         *     This is the the part of an unparsed URI consisting
         *     of the authority (if any) followed by the path.
         *
         * @param[out] pathString
         *     This is where to store the the path
         *     part of the input string.
         *
         * @return
         *     An indication of whether or not the given input string
         *     was successfully parsed is returned.
         */

        bool SplitAuthorityFromPathAndParseIt(std::string authorityAndPathString,
                                                std::string& pathString)
        {
            /**
                ADDITIONAL
                ===========

                * Need mutable std::string authorityAndPathString
                * So pass as above instead of const std::string& authorityAndPathString

                * Need pathString to external reference.
                * So pass it as a reference and modify (without const)
            */
            
            if (authorityAndPathString.substr(0, 2) == "//")
            {

                // Strip off authority marker
                authorityAndPathString = authorityAndPathString.substr(2);

                auto authorityEnd = authorityAndPathString.find_first_of("/?#"); // find '/' from schemeEnd+3 point onards - find(character(s), starting point)

                //if (authorityEnd == std::string::npos) // npos likes compare with -1
                //{
                //    authorityEnd = authorityAndPathString.find('?', 2);
                //}

                //if (authorityEnd == std::string::npos) // npos likes compare with -1
                //{
                //    authorityEnd = authorityAndPathString.find('#', 2);
                //}

                if (authorityEnd == std::string::npos) // npos likes compare with -1
                {
                    authorityEnd = authorityAndPathString.length();
                }

                // Seperate authorityAndPathString into two chunks pathString & authorityString
                pathString = authorityAndPathString.substr(authorityEnd);
                auto authorityString = authorityAndPathString.substr(0, authorityEnd);

                // Parse the elements inside the authority string
                if (!ParseAuthority(authorityString))
                {
                    return false;
                }
            }
            else
            {
                host.clear();
                userInfo.clear();
                hasPort = false;
                pathString = authorityAndPathString;
            }
            return true;
        }

        // Handle special case of URI with authority and empty
        // path -- treat the same as "/" path.

        /**
         * This method handles the special case of the URI having an
         * authority but having an empty path.  In this case it sets
         * the path as "/".
         */
        void SetDefaultPathIfAuthorityPresentAndPathEmpty()
        {
            if (!host.empty() && path.empty())
            {
                path.push_back("");
            }
        }
        /**
         * This method takes the part of a URI string that has just
         * the query and/or fragment elements, and breaks off
         * and decodes the fragment part, returning the rest,
         * which will be either empty or have the query with the
         * query delimiter still attached.
         *
         * @param[in] queryAndOrFragment
         *     This is the part of a URI string that has just
         *     the query and/or fragment elements.
         *
         * @param[out] rest
         *     This is where to store the rest of the input string
         *     after removing any fragment and fragment delimiter.
         *
         * @return
         *     An indication of whether or not the method succeeded
         *     is returned.
         */
        bool ParseFragment(const std::string& queryAndOrFragment, std::string& rest)
        {
            // Next, parse the fragment if there is one.
            const auto fragmentDelimiter = queryAndOrFragment.find('#');
            if (fragmentDelimiter == std::string::npos) {
                fragment.clear();
                rest = queryAndOrFragment;
            }
            else {
                fragment = queryAndOrFragment.substr(fragmentDelimiter + 1);
                rest = queryAndOrFragment.substr(0, fragmentDelimiter);
            }
            /**
            ADDITIONAL
            ===========
            This is called a tail call.

            Return is calling something and then returning back out to you
             */
            return DecodeQueryOrFragment(fragment);
        }


        /**
         * This method takes the part of a URI string that has just
         * the query element with its delimiter, and breaks off
         * and decodes the query.
         *
         * @param[in] queryWithDelimiter
         *     This is the part of a URI string that has just
         *     the query element with its delimiter.
         *
         * @return
         *     An indication of whether or not the method succeeded
         *     is returned.
         */
        bool ParseQuery(const std::string& queryWithDelimiter)
        {
            if (queryWithDelimiter.empty())
            {
                query.clear();
            }
            else
            {
                query = queryWithDelimiter.substr(1);
            }
            return DecodeQueryOrFragment(query);

        }

        /**
         * This method determines whether or not it makes sense to
         * navigate one level up from the current path
         * (in other words, does appending ".." to the path
         * actually change the path?)
         *
         * @return
         *     An indication of whether or not it makes sense to
         *     navigate one level up from the current path is returned.
         */


         /**
          * ADDITIONAL
           ============
           * auto can avoid repetition of names of types
           * type inference can be overused
                                    Type inference is the compile-time process of reconstructing
            missing type information in a program based on the usage of its variables

         */

         /**
          * ADDITIONAL
           ============
           * In here it covers the entire super set URI (Uniform Resource Identifier)
           * It can be a Path in the internet (http, website, and the path within the website
           * Or you can have a name

           ex:
           * /./g
           * http://a/g
            - This starts with http://, but this is not really relevant
            - You can strike that and same rules apply
            - It can be C/, it make more like a file system path like in windows
            - Same rules apply

           * When have . or .. you normalize and collapse so that you remove the relative navigation
           * 
          */
        bool CanNavigatePathUpOneLevel() const {
            return (
                !IsPathAbsolute()
                || (path.size() > 1)
                );
        }

        /**
         * This method applies the "remove_dot_segments" routine talked about
         * in RFC 3986 (https://tools.ietf.org/html/rfc3986) to the path
         * segments of the URI, in order to normalize the path
         * (apply and remove "." and ".." segments).
         */
        void NormalizePath()
        {
            // Rebuild the path one segment
            // at a time, removing and applying special
            // navigation segments ("." and "..") as we go.

            auto oldPath = std::move(path);
            path.clear();
            //bool isAbsolute = (
            //    !oldPath.empty()
            //    && oldPath[0].empty()
            //    );
            bool atDirectoryLevel = false;
            for (const auto segment : oldPath) {
                if (segment == ".") {
                    atDirectoryLevel = true;
                }
                else if (segment == "..") {
                    // Remove last path element
                    // if we can navigate up a level.
                    if (!path.empty()) {
                        if(CanNavigatePathUpOneLevel())
                        {
                        //if (
                        //    !isAbsolute
                        //    || (path.size() > 1)
                        //    ) {
                            path.pop_back();
                        }
                    }
                    atDirectoryLevel = true;
                }
                else {
                    // Non-relative elements can just
                    // transfer over fine.  An empty
                    // segment marks a transition to
                    // a directory level context.  If we're
                    // already in that context, we
                    // want to ignore the transition.
                    if (
                        !atDirectoryLevel
                        || !segment.empty()
                        ) {
                        path.push_back(segment);
                    }
                    atDirectoryLevel = segment.empty();
                }
            }

            // If at the end of rebuilding the path,
            // we're in a directory level context,
            // add an empty segment to mark the fact.
            if (
                atDirectoryLevel
                && (
                    !path.empty()
                    && !path.back().empty()
                    )
                ) {
                path.push_back("");
            }
        }

        /**
         * This method replaces the URI's scheme with that of
         * another URI.
         *
         * @param[in] other
         *     This is the other URI from which to copy the scheme.
         */
        void CopyScheme(const Uri& other) {
            scheme = other.impl_->scheme;
        }

        /**
         * This method replaces the URI's authority with that of
         * another URI.
         *
         * @param[in] other
         *     This is the other URI from which to copy the authority.
         */
        void CopyAuthority(const Uri& other) {
            host = other.impl_->host;
            userInfo = other.impl_->userInfo;
            hasPort = other.impl_->hasPort;
            port = other.impl_->port;
        }

        /**
         * This method replaces the URI's path with that of
         * another URI.
         *
         * @param[in] other
         *     This is the other URI from which to copy the path.
         */
        void CopyPath(const Uri& other) {
            path = other.impl_->path;
        }

        /**
         * This method replaces the URI's path with that of
         * the normalized form of another URI.
         *
         * @param[in] other
         *     This is the other URI from which to copy
         *     the normalized path.
         */
        void CopyAndNormalizePath(const Uri& other) {
            CopyPath(other);
            NormalizePath();
        }

        /**
         * This method replaces the URI's query with that of
         * another URI.
         *
         * @param[in] other
         *     This is the other URI from which to copy the query.
         */
        void CopyQuery(const Uri& other) {
            query = other.impl_->query;
        }

        /**
         * This method replaces the URI's fragment with that of
         * another URI.
         *
         * @param[in] other
         *     This is the other URI from which to copy the query.
         */
        void CopyFragment(const Uri& other) {
            fragment = other.impl_->fragment;
        }

        /**
         * This method returns an indication of whether or not the
         * path of the URI is an absolute path, meaning it begins
         * with a forward slash ('/') character.
         *
         * @return
         *     An indication of whether or not the path of the URI
         *     is an absolute path, meaning it begins
         *     with a forward slash ('/') character is returned.
         */
        bool IsPathAbsolute() const {
            return (
                !path.empty()
                && (path[0] == "")
                );
        }
    };

    Uri::~Uri() = default;

    Uri::Uri(Uri&&) = default;

    Uri& Uri::operator=(Uri&&) = default;

    Uri::Uri()
        : impl_(new Impl)
    {

    }

    bool Uri::operator==(const Uri& other) const
    {
        return (
            (impl_->scheme == other.impl_->scheme) &&
            (impl_->userInfo == other.impl_->userInfo) &&
            (impl_->host == other.impl_->host) &&
            (
            (!impl_->hasPort == !other.impl_->hasPort) ||
                (
                (impl_->hasPort == other.impl_->hasPort) &&
                    (impl_->port == other.impl_->port)
                    )
                ) &&
                (impl_->path == other.impl_->path) &&
            (impl_->query == other.impl_->query) &&
            (impl_->fragment == other.impl_->fragment)
            );
    }

    bool Uri::operator!=(const Uri& other) const
    {
        return !(*this == other);
    }

    // to get one method to the other, we use state

    // ex: "http://www.example.com/foo/bar"
    bool Uri::ParseFromString(const std::string& uriString)
    {
        resetImpl_();

        /**
        ADDITIONAL
        -----------
        * Refactored to private methods one readable even without comments

        */

        // First pass the scheme
        std::string rest;
        
        if (!impl_->ParseScheme(uriString, rest))
        {
            return false;
        }


        // Split authority from path. If there is an authority, parse it.

        //if (uriString.substr(schemeEnd+1, 2) == "//")
        //{
        //    const auto authorityEnd = uriString.find('/', schemeEnd + 3); // find '/' from schemeEnd+2 point onards - find(character(s), starting point)
        //    impl_->host = uriString.substr(schemeEnd + 3, authorityEnd - (schemeEnd + 3)); // substr(starting point, length)
        //}
        //else
        //{
        //    impl_->host.clear();
        //}

        auto pathEnd = rest.find_first_of("?#");

        if (pathEnd == std::string::npos) // npos likes compare with -1
        {
            pathEnd = rest.length();
        }

        auto authorityAndPathString = rest.substr(0, pathEnd);

        std::string queryAndOrFragment;

        queryAndOrFragment = rest.substr(authorityAndPathString.length()); // Copy sub str after pathString.length() position [substr(starting position)]

        std::string pathString;
        if (!impl_->SplitAuthorityFromPathAndParseIt(authorityAndPathString, pathString))
        {
            return false;
        }


        // Next pass the path

        /*Expected outcomes
            "" -> []
            "/" -> [""]
            "/foo" -> ["foo"]
            "foo/" -> ["foo", ""]
        */
        //auto pathString = hostPortAndPathString;

        if (!impl_->ParsePath(pathString))
        {
            return false;
        }


        //impl_->SetDefaultPathIfAuthorityPresentAndPathEmpty();
        if (!impl_->host.empty() && impl_->path.empty())
        {
            impl_->path.push_back("");
        }

        // Next, parse the fragment if there is one.
        if (!impl_->ParseFragment(queryAndOrFragment, rest))
        {
            return false;
        }

        // Finally, if anything is left, it's the query.

        return impl_->ParseQuery(rest);
    }


    std::string Uri::GetScheme() const
    {
        return impl_->scheme;
    }


    std::string Uri::GetHost() const
    {
        return impl_->host;
    }


    std::vector<std::string> Uri::GetPath() const
    {
        return impl_->path;
    }


    bool Uri::HasPort() const
    {
        return impl_->hasPort;
    }

    uint16_t Uri::GetPort() const
    {
        return impl_->port;
    }

    void Uri::resetImpl_() const
    {
        impl_->scheme.clear();
        impl_->host.clear();
        impl_->path.clear();
        impl_->hasPort = false;
        impl_->port = 0;
        impl_->userInfo.clear();
        impl_->query.clear();
        impl_->isRelativeReference = false;
        impl_->fragment.clear();
    }

    bool Uri::IsRelativeReference() const
    {
        return impl_->scheme.empty();
    }

    bool Uri::ContainsRelativePath() const
    {
        //if (impl_->path.empty())
        //{
        //    return true;
        //}
        //else
        //{
        //    return !impl_->path[0].empty();
        //}
        return !impl_->IsPathAbsolute();
    }

    std::string Uri::GetQuery() const
    {
        return impl_->query;
    }

    std::string Uri::GetFragment() const
    {
        return impl_->fragment;
    }

    std::string Uri::GetUserInfo() const
    {
        return impl_->userInfo;
    }

    //void Uri::NormalizePath()
    //{
    //    auto oldPath = std::move(impl_->path); // input buffer
    //    impl_->path.clear(); // output buffer

    //    bool isAbsolute = (!oldPath.empty() && oldPath[0].empty());

    //    bool atDirectoryLevel = false;

    //    for (const auto segment : oldPath)
    //    {
    //        if (segment == ".")
    //        {
    //            atDirectoryLevel = true;
    //        }
    //        else if (segment == "..")
    //        {
    //            if (!impl_->path.empty())
    //            {
    //                if (!isAbsolute || impl_->path.size() > 1)
    //                {
    //                    impl_->path.pop_back();
    //                }
    //            }
    //            atDirectoryLevel = true;
    //        }
    //        else
    //        {
    //            if (!atDirectoryLevel || !segment.empty())
    //            {
    //                impl_->path.push_back(segment);
    //            }
    //            atDirectoryLevel = segment.empty();
    //        }
    //    }

    //    if (
    //        atDirectoryLevel &&
    //        (
    //            !impl_->path.empty()
    //            && !impl_->path.back().empty()
    //            )
    //        )
    //    {
    //        impl_->path.push_back("");
    //    }
    //}
    //void Uri::NormalizePath()
    //{
    //    auto oldPath = std::move(impl_->path); // input buffer
    //    impl_->path.clear(); // output buffer

    //    for (size_t i = 0; i < oldPath.size(); ++i)
    //    {
    //        const auto segment = oldPath[i];

    //        if (segment == ".")
    //        {
    //            if (i + 1 < oldPath.size())
    //            {
    //                continue;
    //            }
    //        }
    //        else if (segment == "..")
    //        {
    //            if (
    //                    !impl_->path.empty() &&
    //                    (
    //                        (impl_->path.size() > 1) ||
    //                        (impl_->path[0].empty())
    //                    )
    //                )
    //            {
    //                impl_->path.pop_back();
    //            }
    //        }
    //        else
    //        {
    //            impl_->path.push_back(segment);
    //        }
    //    }
    //}
    //void Uri::NormalizePath()
    //{
    //    /*
    //     * This is a straight-up implementation of the
    //     * algorithm from section 5.2.4 of
    //     * RFC 3986 (https://tools.ietf.org/html/rfc3986).
    //     */

    //     // Step 1
    //    auto oldPath = std::move(impl_->path); // input buffer
    //    impl_->path.clear(); // output buffer

    //    // Step 2
    //    while (!oldPath.empty())
    //    {
    //        // Step 2A
    //        if ((oldPath[0] == ".") || (oldPath[0] == ".."))
    //        {
    //            oldPath.erase(oldPath.begin());
    //        }
    //        // Step 2B
    //        else if ((oldPath.size() >= 2) && (oldPath[0] == "") && (oldPath[1] == "."))
    //        {
    //            /**

    //             * /./a => "", ".", "a"
    //             * /. => "", "."
    //             * replace that /./ prefix with "/" means 
    //             * /a => "", "a"
    //             * / => ""

    //             * simply removing "." of path segments is enough

    //             */
    //            oldPath.erase(oldPath.begin()+1);
    //        }
    //        // Step 2C
    //        else if ((oldPath.size() >= 2) && (oldPath[0] == "") && (oldPath[1] == ".."))
    //        {
    //            /**

    //             * input buffer = /../a => "", "..", "a"       output buffer = b
    //             * /.. => "", ".."                             output buffer = b
    //             * replace that /../ prefix with "/" means
    //             * input buffer = /a => "", "a"                output buffer = 
    //             * input buffer = / => ""                      output buffer =

    //             * simply removing ".." of path segments from input buffer
    //             * and pop last segment of output buffer is anough

    //             */
    //            oldPath.erase(oldPath.begin() + 1);
    //            if (!impl_->path.empty())
    //            {
    //                impl_->path.pop_back();
    //            }
    //        }
    //        // Step 2D
    //        else if ((oldPath.size() == 1) && (oldPath[0] == ".") && (oldPath[1] == ".."))
    //        {
    //            oldPath.erase(oldPath.begin());
    //        }
    //        // Step 2E
    //        else
    //        {
    //            // move the first path segment in the input buffer to the end of
    //            // the output buffer
    //            if (oldPath[0] == "")
    //            {
    //                // including the initial "/" character(if any) and any subsequent characters up to
    //                if (impl_->path.empty())
    //                {
    //                    impl_->path.push_back("");
    //                }

    //                //but not including, the next "/" character or the end of the input buffer
    //                oldPath.erase(oldPath.begin());

    //            }
    //            if (!oldPath.empty())
    //            {
    //                impl_->path.push_back(oldPath[0]);

    //                if (oldPath.size() >= 1)
    //                {
    //                    oldPath[0] = "";
    //                }
    //                else
    //                {
    //                    oldPath.erase(oldPath.begin());
    //                }
    //            }
    //        }
    //    }
    //}

    void Uri::NormalizePath()
    {
        impl_->NormalizePath();
    }

    Uri Uri::Resolve(const Uri& relativeReference) const
    {
        // Resolve the reference by following the algorithm
        // from section 5.2.2 in
        // RFC 3986 (https://tools.ietf.org/html/rfc3986).

        Uri target;

        if (!relativeReference.impl_->scheme.empty())
        {
            //target.impl_->scheme = relativeReference.impl_->scheme;

            ////authority = host, userInfo, port
            //target.impl_->host = relativeReference.impl_->host;
            //target.impl_->userInfo = relativeReference.impl_->userInfo;
            //target.impl_->hasPort = relativeReference.impl_->hasPort;

            //target.impl_->port = relativeReference.impl_->port;
            //target.impl_->path = relativeReference.impl_->path;
            //target.NormalizePath();
            //target.impl_->query = relativeReference.impl_->query;

            target.impl_->CopyScheme(relativeReference);
            target.impl_->CopyAuthority(relativeReference);
            target.impl_->CopyAndNormalizePath(relativeReference);
            target.impl_->CopyQuery(relativeReference);
        }
        else
        {
            if (!relativeReference.impl_->host.empty())
            {
                ////authority = host, userInfo, port
                //target.impl_->host = relativeReference.impl_->host;
                //target.impl_->userInfo = relativeReference.impl_->userInfo;
                //target.impl_->hasPort = relativeReference.impl_->hasPort;
                //target.impl_->port = relativeReference.impl_->port;

                //target.impl_->path = relativeReference.impl_->path;
                //target.NormalizePath();
                //target.impl_->query = relativeReference.impl_->query;
                target.impl_->CopyAuthority(relativeReference);
                target.impl_->CopyAndNormalizePath(relativeReference);
                target.impl_->CopyQuery(relativeReference);
            }
            else
            {
                if (relativeReference.impl_->path.empty())
                {
                    target.impl_->path = this->impl_->path;

                    if (!relativeReference.impl_->query.empty())
                    {
                        //target.impl_->query = relativeReference.impl_->query;
                        target.impl_->CopyQuery(relativeReference);
                    }
                    else
                    {
                        //target.impl_->query = this->impl_->query;
                        target.impl_->CopyQuery(*this);
                    }
                }
                else
                {
                    // RFC describes this as:
                    // "if (R.path starts-with "/") then"
                    if (relativeReference.impl_->IsPathAbsolute())
                    {
                        /*target.impl_->path = relativeReference.impl_->path;
                        target.NormalizePath();*/
                        target.impl_->CopyAndNormalizePath(relativeReference);
                    }
                    else
                    {
                        //T.path = merge(Base.path, R.path);
                        //T.path = remove_dot_segments(T.path);

                        /**
                            ADDITIONAL
                            -----------

                            1 method to concatenate two vectors
                            ------------------------------------

                            target.impl_->path.clear();
                            target.impl_->path.reserve(
                                this->impl_->path.size()
                                + relativeReference.impl_->path.size()
                                -2
                            );

                            target.impl_->path.insert(
                                target.impl_->path.end(),
                                this->impl_->path.begin(),
                                this->impl_->path.end()-1
                            );

                            target.impl_->path.insert(
                                target.impl_->path.end(),
                                relativeReference.impl_->path.begin(),
                                relativeReference.impl_->path.end()
                            );
                        */

                        // RFC describes this as:
                        // "T.path = merge(Base.path, R.path);"
                        /*target.impl_->path = impl_->path;*/
                        target.impl_->CopyPath(*this);

                        if (target.impl_->path.size() > 1)
                        {
                            target.impl_->path.pop_back();
                        }

                        std::copy(
                            relativeReference.impl_->path.begin(),
                            relativeReference.impl_->path.end(),

                            /**
                            ADDITIONAL
                            -----------
                            * Back Inserter into target

                            * For every element in relativeReference begining to and as defined,
                            We are going to give to this back inserter.

                            * It takes the target path and insert every element at back

                            * So effectively concatenating two vectors

                            */
                            std::back_inserter(target.impl_->path)
                        );
                        target.NormalizePath();
                    }
                    //target.impl_->query = relativeReference.impl_->query;
                    target.impl_->CopyQuery(relativeReference);
                }
                ////authority = host, userInfo, port
                //target.impl_->host = this->impl_->host;
                //target.impl_->userInfo = this->impl_->userInfo;
                //target.impl_->hasPort = this->impl_->hasPort;
                //target.impl_->port = this->impl_->port;
                target.impl_->CopyAuthority(*this);
            }
            target.impl_->CopyScheme(*this);
        }
        target.impl_->CopyFragment(relativeReference);

        return target;
    }


    void Uri::SetScheme(const std::string& scheme) {
        impl_->scheme = scheme;
    }

    void Uri::SetHost(const std::string& host) {
        impl_->host = host;
    }

    void Uri::SetQuery(const std::string& query) {
        impl_->query = query;
    }

    std::string Uri::GenerateString() const {
        std::ostringstream buffer;
        if (!impl_->scheme.empty()) {
            buffer << impl_->scheme << ':';
        }
        if (!impl_->host.empty()) {
            buffer << "//";
            if (ValidateIpv6Address(impl_->host)) {
                buffer << '[' << impl_->host << ']';
            }
            else {
                buffer << impl_->host;
            }
        }
        if (!impl_->query.empty()) {
            buffer << '?' << impl_->query;
        }
        return buffer.str();
    }
}


