/**
 * @file CharacterSet.cpp
 *
 * This module contains the implementation of the
 * Uri::CharacterSet class.
 *
 */

#include "CharacterSet.hpp"
#include <set>

namespace Uri
{
    /**
     * This contains the private properties of the CharacterSet class.
     */
    struct CharacterSet::Impl {
        /**
         * This holds the characters in the set.
         */
        std::set< char > charactersInSet;
    };

    CharacterSet::~CharacterSet() = default;

    // copy constructor
    /**
    Additional - copy constructor
    ===========
        * Deep copy
        * can't use the same pointer value in both original and copied opbject
        * so need to override the default copy constructor
    */

    CharacterSet::CharacterSet(const CharacterSet& other)
        : impl_(new Impl(*other.impl_))
    {
    }
    CharacterSet::CharacterSet(CharacterSet&& other) = default;

    // copy assignment
    /**
    Additional - copy assignment
    ===========
        * can't use the same pointer value in both original and copied opbject
        * without copying the pointer value, copy the pointers object value and assign
    */
    CharacterSet& CharacterSet::operator=(const CharacterSet& other) {
        if (this != &other) {
            *impl_ = *other.impl_;
        }
        return *this;
    }

    CharacterSet& CharacterSet::operator=(CharacterSet&& other) = default;



    CharacterSet::CharacterSet()
        : impl_(new Impl)
    {
    }

    CharacterSet::CharacterSet(char c)
        : impl_(new Impl)
    {
        (void)impl_->charactersInSet.insert(c);
    }

    CharacterSet::CharacterSet(char first, char last)
        : impl_(new Impl)
    {
        for (char c = first; c < last + 1; ++c) {
            (void)impl_->charactersInSet.insert(c);
        }
    }

    CharacterSet::CharacterSet(
        std::initializer_list< const CharacterSet > characterSets
    )
        : impl_(new Impl)
    {
        /*
        ADDITIONAL - set - O(log n)
        ==========

        set_name.insert(element)
        set_name.insert(second_set.begin_position(), second_set.end_position())

        The time complexity of set operations is O(log n)

        */

        for (
            auto characterSet  = characterSets.begin();
            characterSet != characterSets.end();
            ++characterSet
            )
        {
            impl_->charactersInSet.insert(
                characterSet->impl_->charactersInSet.begin(),
                characterSet->impl_->charactersInSet.end()
            );
        }
    }

    bool CharacterSet::Contains(char c) const {
        return impl_->charactersInSet.find(c) != impl_->charactersInSet.end();
    }

}
