#ifndef URI_PERCENT_ENCODED_CHARACTER_DECODER_HPP
#define URI_PERCENT_ENCODED_CHARACTER_DECODER_HPP

/**
 * @file PercentEncodeCharacterDecoder.hpp
 *
 * This module declares the Uri::PercentEncodeCharacterDecoder class.
 *
 */

#include <memory>
#include <stddef.h>

namespace Uri
{

    /*
        ADDITIONAL
        ==========
        * Return type can use as an failure indicator (bool)
        * Alternative way is touse exceptions
        * Embedded like systems might have limitations to use some language features like excpetions
        * But if need more info about the failure/error string, can use exception to get the failure info

        bool DecodePercentEncodeCharacter(const std::)
    */

    /*
        ADDITIONAL
        ==========
        * It may be upto the dev to decide on whther using a function or whether use a class
        * Make an object from this and push characters to it until it says done
    */

    /*
        ADDITIONAL
        ==========
        * Struct vs Class -
            struct members and base classes/structs are public by default.
            class members and base classes/struts are private by default.

        * Recommended:
            use struct for plain-old-data (POD) structures without any class-like features;
            use class when you make use of features such as private or protected members, non-default constructors and operators, etc.

        * POD
            In a simple way, POD stands for Plain Old Data - that is, a class (whether defined with the keyword struct or the keyword class) without constructors, destructors and virtual members functions.
            In short, it is all built-in data types (e.g. int, char, float, long, unsigned char, double, etc.) and all aggregation of POD data.
    */

    /*
        ADDITIONAL
        ==========
        * Instead of a struct lets use a formal class. Then we can have some private properties
        * Good refactoring keep the class in a speerate file
    */


    /**
     * This class can take in a percent-encoded character,
     * decode it, and also detect if there are any problems in the encoding.
     */

    class PercentEncodeCharacterDecoder
    {
    /**
        // ADDITIONAL
        ===============
        // Lifecycle management
        // these 5 refers to as rules of zero or rule of 5
        // if need one of them define all 5
        // if not need any one of them can ignore
        // It just make everything consisting as far as destroying copying or moving an object

        (
        Reason - here because of Impl we can't default the constructor and destructor.
        So we need a change to that default behaviour
        When changing default behaviour of one of these 5, have consider other 4 too
        We just deleting other 4 here as they don't need yet
        )
    */

    public:
        ~PercentEncodeCharacterDecoder();
        // delete for remove the implementation

        //Copy constructor
        PercentEncodeCharacterDecoder(const PercentEncodeCharacterDecoder&) = default;
        //Move constructor
        PercentEncodeCharacterDecoder(PercentEncodeCharacterDecoder&&);
        //Copy assignment
        PercentEncodeCharacterDecoder& operator=(const PercentEncodeCharacterDecoder&) = delete;
        //Move assignment
        PercentEncodeCharacterDecoder& operator=(PercentEncodeCharacterDecoder&&);

        // Method
    public:
        /**
         * This is the default constructor.
         * One major reason for the constructor is that the
         Impl unique pointer need specified constructore and destructor
         * Constructor build the Impl pointer first place and
         destructor needs to be concrete and default in the header
         */
        PercentEncodeCharacterDecoder();


        /*
            ADDITIONAL
            ==========
            // It is upto the developer to decide on whether they need to worry about GetDecodedCharacter calls after done like info
            // means worrying about is it calling all the dependecies in the given order
        */

        /**
         * This method inputs the next encoded character.
         *
         * @param[in] c
         *     This is the next encoded character to give to the decoder.
         *
         * @return
         *     An indication of whether or not the encoded character
         *     was accepted is returned.
         */
        bool NextEncodedCharacter(char c);

        /**
         * This method checks to see if the decoder is done
         * and has decoded the encoded character.
         *
         * @return
         *     An indication of whether or not the decoder is done
         *     and has decoded the encoded character is returned.
         */
        bool Done() const;

        /**
         * This method returns the decoded character, once
         * the decoder is done.
         *
         * @return
         *     The decoded character is returned.
         */
        char GetDecodedCharacter() const;


        // Properties
    private:


        /**
          * This is the type of structure that contains the private
          * properties of the instance.  It is defined in the implementation
          * and declared here to ensure that it is scoped inside the class.
          */
        struct Impl;

        /**
         * This contains the private properties of the instance.
         * This gives the 'piml' pattern implementation in C++
         * by making private pointer

         * "Pointer to implementation" or "pImpl" is a C++ programming technique
         that removes implementation details of a class from its object representation
         by placing them in a separate class, accessed through an opaque pointer:

         * Can include non-virtual methods too
           
         */
        std::unique_ptr< struct Impl > impl_;
    };
}

#endif /* URI_PERCENT_ENCODED_CHARACTER_DECODER_HPP */
