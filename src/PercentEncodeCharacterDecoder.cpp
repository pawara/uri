/**
 * @file PercentEncodeCharacterDecoder.cpp
 *
 * This module contains the implementation of the
 * Uri::PercentEncodeCharacterDecoder class.
 *
 */

#include "PercentEncodeCharacterDecoder.hpp"
#include "CharacterSet.hpp"

namespace Uri
{

    /**
     * This is the character set containing just numbers.
     */
     // this don't need initializer list, need fisrt last char constructor to call
    const Uri::CharacterSet DIGIT('0', '9');

    /**
     * This is the character set containing just the upper-case
     * letters 'A' through 'F', used in upper-case hexadecimal.
     */
    const Uri::CharacterSet HEX_UPPER('A', 'F');

    /**
     * This is the character set containing just the lower-case
     * letters 'a' through 'f', used in lower-case hexadecimal.
     */
    const Uri::CharacterSet HEX_LOWER('a', 'f');


    struct PercentEncodeCharacterDecoder::Impl
    {
        //Properties

        /*
            ADDITIONAL
            ==========
            //lets use _ to indicate it is a private member of the class
            //Some people use
                        m_decodedCharacter
                        m_cDecodedCharacter - member variable character
                        impl_->decodedCharacter
        */

        /*
            ADDITIONAL
            ==========
            //can initialize at the declaration as below
        */

        /**
         * This is the decoded character.
         */
        char decodedCharacter = 0;

        /**
         * This is the number of digits that we still need to shift in
         * to decode the character.
         */
        size_t digitsLeft = 2;

        // Methods
        /**
         * This method shifts in the given hex digit as part of
         * building the decoded character.
         *
         * @param[in] c
         *     This is the hex digit to shift into the decoded character.
         *
         * @return
         *     An indication of whether or not the given hex digit
         *     was valid is returned.
         */
        bool ShiftInHexDigit(char c)
        {
            decodedCharacter <<= 4;

            if (DIGIT.Contains(c))
            {
                decodedCharacter += (int)(c - '0');

            }
            else if (HEX_UPPER.Contains(c))
            {
                decodedCharacter += (int)(c - 'A') + 10;
            }
            else if (HEX_LOWER.Contains(c))
            {
                decodedCharacter += (int)(c - 'a') + 10;
            }
            else
            {
                return false;
            }
            return true;
        }
    };

    PercentEncodeCharacterDecoder::~PercentEncodeCharacterDecoder() = default;
    /*
        ADDITIONAL
        ==========
        * unique pointers support moves
        * Anonymous namespace (anonymous namespace function) is a namespace without a name
            - Compiler assigns like a random string
            - It doesn't collide with functions in other modules (with anonymous namespaces)
            - Prevent colliding at linker step
            - Actually namespaces given a name which is only visible to the linker and the compiler
            - Programmer can't see it
            - So can't reach a function which is not possible to reach
            - Anonymous namespace can access outside ofthe anonymouys namespaces,
            but only within the same cpp file (something like private only to that CPP file)
    */

    /*
    ADDITIONAL
    ==========
        //TODO - reason why can't declare as default in header
    */
    //Move constructor
    PercentEncodeCharacterDecoder::PercentEncodeCharacterDecoder(PercentEncodeCharacterDecoder&&) = default;

    //Move assignment
    PercentEncodeCharacterDecoder& PercentEncodeCharacterDecoder::operator=(PercentEncodeCharacterDecoder&&) = default;

    PercentEncodeCharacterDecoder::PercentEncodeCharacterDecoder() : impl_(new Impl)
    {

    }

    /*
    ADDITIONAL
    ==========
        even says to check pre consitions in every function,
        in real world it is best to do all your checking closest to the untrust worthy source.

        For this - untrust worthy source is input to URI.
        By the time we know URI code is already well tested that is only going to allowed 2 characters to be shifted in
        and it is going to be calling done to tell done

    */
    bool PercentEncodeCharacterDecoder::NextEncodedCharacter(char c)
    {
        if (!impl_->ShiftInHexDigit(c)) {
            return false;
        }
        --impl_->digitsLeft;
        return true;
    }

    bool PercentEncodeCharacterDecoder::Done() const {
        return (impl_->digitsLeft == 0);
    }

    char PercentEncodeCharacterDecoder::GetDecodedCharacter() const
    {
        return (char)impl_->decodedCharacter;
    }
    /*
    ADDITIONAL 
    ==========
        Based on the requirement, where and how the code will be using can decide on the pre conditions and validations.

        A Rule following within this code is that, do not double check pre conditions and all that things
        May be at academic level tells to always check pre conditions in every function
        Real world, it might best to always checking closes to the untruct worthy source.

        Here untruct worthy source is input to URI.
        By the time make start to here, that know URI is already well tested
        and that only going to allow two characters to be shifted in.
        And it is going to calling done to make detect all your work done.

        No need to check NextEncodedCharacter call three time.

        This uses only within URI as a private. And checking might be redundant.
        And it can take the decision based on preference, other factors, where and how this will be using. 
    */

}
