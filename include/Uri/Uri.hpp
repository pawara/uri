#ifndef URI_HPP
#define URI_HPP

/**
 * @file Uri.hpp
 *
 * This module declares the Uri::Uri class.
 *
 */

#include <memory>
#include <string>
#include <vector>

namespace Uri {

    /**
     * This class represents a Uniform Resource Identifier (URI),
     * as defined in RFC 3986 (https://tools.ietf.org/html/rfc3986).
     */
    class Uri {
    /**
        // ADDITIONAL
        ===============
        // Lifecycle management
        // these 5 refers to as rules of zero or rule of 5
        // if need one of them define all 5
        // if not need any one of them can ignore
        // It just make everything consisting as far as destroying copying or moving an object

        (
        Reason - here because of Impl we can't default the constructor and destructor.
        So we need a change to that default behaviour
        When changing default behaviour of one of these 5, have consider other 4 too
        We just deleting other 4 here as they don't need yet
        )
     */
    public:
        ~Uri();
        // delete for remove the implementation

        /**
            // ADDITIONAL - Copy constructor
            ==============
            Default Copy Constructor :

                C++ compiler will create a default constructor which copies all the member variables as it is when the copy constructor is not defined.

            User - Defined Copy Constructor :

                The user defines the user - defined Copy constructor.

                Syntax for user defined Copy constructor :

                Class_Name(Class_Name &obj) {

                    // body of constructor 

                }

                - Here obj is the reference that is being initialised to another object.

                Uses of Copy Constructor
                -------------------------
                - When we initialize an object by another object of the same class.
                - When we return an object as a function value.
                - When the object is passed to a function as a non - reference parameter.
         */

        //Copy constructor
        Uri(const Uri&) = delete;
        //Move constructor
        Uri(Uri&&);
        //Copy assignment
        Uri& operator=(const Uri&) = delete;
        //Move assignment
        Uri& operator=(Uri&&);

        // Public methods
    public:
        /**
         * This is the default constructor.
         */
        Uri();

        /**
         * This is the equality comparison operator for the class.
         *
         * @param[in] other
         *     This is the other URI to which to compare this URI.
         *
         * @return
         *     An indication of whether or not the two URIs are
         *     equal is returned.
         */
        bool operator==(const Uri& other) const;


        /**
         * This is the inequality comparison operator for the class.
         *
         * @param[in] other
         *     This is the other URI to which to compare this URI.
         *
         * @return
         *     An indication of whether or not the two URIs are
         *     not equal is returned.
         */
        bool operator!=(const Uri& other) const;

        /**
         * This method builds the URI from the elements parsed
         * from the given string rendering of a URI
         *
         * @param[in] uriString
         *      This is the string rendering of the URI to parse.
         *
         * @return
         *      An idication of whether or not the URI was
         *      parsed successfully is returned
         */

        //pass a const string to ensure uri doesn't change from the function 
        bool ParseFromString(const std::string& uriString);




        /**
         * This method returns the "scheme" element of the URI
         *
         * @return
         *      The "scheme" element of the URI returned
         * @retVal ""
         *      This is returned if there is no "scheme" element in the URI.
         */

        // const objects can call only const functions
        // but const function can call by a non-const obj
        std::string GetScheme() const;



        /**
         * This method returns the "UserInfo" element of the URI.
         *
         * @return
         *     The "UserInfo" element of the URI is returned.
         *
         * @retval ""
         *     This is returned if there is no "UserInfo" element in the URI.
         */
        std::string GetUserInfo() const;




        /**
         * This method returns the "host" element of the URI
         *
         * @return
         *      The "host" element of the URI returned
         * @retVal ""
         *      This is returned if there is no "host" element in the URI.
         */

         // const objects can call only const functions
         // but const function can call by a non-const obj
        std::string GetHost() const;







        /**
         * This method returns the "path" element of the URI,
         * as a sequence of segments
         *
         * @note
         *      If the first step of the Path is an empty string,
         *      then the URI has an absolute path.
         * @retVal ""
         *      The "path" element of the URI is returned
         *      as a sequence of segments.
         */

        std::vector<std::string> GetPath() const;







        /**
        * This method returns an indication of hether or not the
        * URI includes a port number
        *
        * @return
        *       an indication of hether or not the URI includes a port number
        */

        bool HasPort() const;







        // port number is a 16-bit unsigned integer, thus ranging from 0 to 65535

        /*

        ADDITIONAL FACTS uint8_t uint16_t int
        =================
        A u prefix means unsigned.
        The number is the number of bits used.There's 8 bits to the byte.
        The _t means it's a typedef.

        The only fuzzy one is int.That is "a signed integer value at the native size for the compiler".
        On an 8 - bit system like the ATMega chips that is 16 bits, so 2 bytes.On 32 - bit systems, like the ARM based Due, it's 32 bits, so 4 bytes.

        */

        /**
         * This method returns the port number element of the URI,
         * if it has one.
         *
         * @return
         *     The port number element of the URI is returned.
         *
         * @note
         *     The returned port number is only valid if the
         *     HasPort method returns true.
         */

        uint16_t GetPort() const;





        /**
         * This method returns an indication of whether or not
         * the URI is a relative reference.
         *
         * @return
         *     An indication of whether or not the URI is a
         *     relative reference is returned.
         */
        bool IsRelativeReference() const;





        /**
         * This method returns an indication of whether or not
         * the URI contains a relative path.
         *
         * @return
         *     An indication of whether or not the URI contains a
         *     relative path is returned.
         */
        bool ContainsRelativePath() const;



        /**
         * This method returns the "query" element of the URI,
         * if it has one.
         *
         * @return
         *     The "query" element of the URI is returned.
         *
         * @retval ""
         *     This is returned if there is no "query" element in the URI.
         */
        std::string GetQuery() const;

        /**
         * This method returns the "fragment" element of the URI,
         * if it has one.
         *
         * @return
         *     The "fragment" element of the URI is returned.
         *
         * @retval ""
         *     This is returned if there is no "fragment" element in the URI.
         */
        std::string GetFragment() const;

        /**
         * This method applies the "remove_dot_segments" routine talked about
         * in RFC 3986 (https://tools.ietf.org/html/rfc3986) to the path
         * segments of the URI, in order to normalize the path
         * (apply and remove "." and ".." segments).
         */
        void NormalizePath();
        // Private properties

        /**
         * This method resolves the given relative reference, based on the given
         * base URI, returning the resolved target URI.
         *
         * @param[in] relativeReference
         *     This describes how to get to the target starting at the base.
         *
         * @return
         *     The resolved target URI is returned.
         *
         * @note
         *     It only makes sense to call this method on an absolute URI
         *     (in which I mean, the base URI should be absolute,
         *     as in IsRelativeReference() should return false).
         */


        Uri Resolve(const Uri& relativeReference) const;

        /*

        ADDITIONAL FACTS const methods
        =================
        member functions which declares as const - cannot modify the base instance

        */

        /*

        ADDITIONAL - macros
        ====================

        part of C pre processor - philosophically can say good or not good

        */

        /**
         * This method sets the scheme element of the URI.
         *
         * @param[in] scheme
         *     This is the scheme to set for the URI.
         */
        void SetScheme(const std::string& scheme);

        /**
         * This method sets the host element of the URI.
         *
         * @param[in] host
         *     This is the host to set for the URI.
         */
        void SetHost(const std::string& host);

        /**
         * This method sets the query element of the URI.
         *
         * @param[in] query
         *     This is the query to set for the URI.
         */
        void SetQuery(const std::string& query);

        /**
         * This method constructs and returns the string
         * rendering of the URI, according to the rules
         * in RFC 3986 (https://tools.ietf.org/html/rfc3986).
         *
         * @return
         *     The string rendering of the URI is returned.
         */
        std::string GenerateString() const;

    private:
        /**
         * This is the type of structure that contains the private
         * properties of the instance.  It is defined in the implementation
         * and declared here to ensure that it is scoped inside the class.
         */
        struct Impl;

        /**
         * This contains the private properties of the instance.
         * This gives the 'piml' pattern implementation in C++
         * by making private pointer

         * "Pointer to implementation" or "pImpl" is a C++ programming technique
         that removes implementation details of a class from its object representation
         by placing them in a separate class, accessed through an opaque pointer:

         Pointer to implementation. Pattern, it is a way in C++ to separate your interface from your implementation. 

         There can be phylosophical arguments about this pattern. 
         */
        std::unique_ptr< struct Impl > impl_;

        void resetImpl_() const;

        
    };

}

#endif /* URI_HPP */
