/**
 * @file UriTests.cpp
 *
 * This module contains the unit tests of the Uri::Uri class.
 *
 */

#include <gtest/gtest.h>
#include <Uri/Uri.hpp>

TEST(UriTests, ParseFromStringUrnNoScheme) {
    Uri::Uri uri;

    ASSERT_TRUE(uri.ParseFromString("foo/bar"));
    ASSERT_EQ("", uri.GetScheme());
    ASSERT_EQ(
        (std::vector<std::string>
    {

            "foo",
            "bar",
    }
    ),
        uri.GetPath()
        );
}
TEST(UriTests, ParseFromStringUrn) {
    Uri::Uri uri;

    ASSERT_TRUE(uri.ParseFromString("http://www.example.com/foo/bar"));
    ASSERT_EQ("http", uri.GetScheme());
    ASSERT_EQ("www.example.com", uri.GetHost());
    ASSERT_EQ(
        (std::vector<std::string>
            {
                "",
                "foo",
                "bar",
            }
        ),
        uri.GetPath()
    );
}

TEST(UriTests, ParseFromStringUrnDefaultPathDelimiter) {
    Uri::Uri uri;

    ASSERT_TRUE(uri.ParseFromString("urn:book:fantasy:Hobbit"));
    ASSERT_EQ("urn", uri.GetScheme());
    ASSERT_EQ("", uri.GetHost());
    ASSERT_EQ(
        (std::vector<std::string>
            {
                "book:fantasy:Hobbit",
            }
        ),
        uri.GetPath()
    );
}

TEST(UriTests, ParseFromStringPathEndCasesPathDelimiter) {

    struct TestVector {
        std::string pathIn;
        std::vector<std::string> pathOut;
    };

    const std::vector<TestVector> testVectors
    {
        {"", {}},
        {"/", {""}},
        {"/foo", {"", "foo"}},
        {"foo/", {"foo", ""}},
    };

    size_t index = 0;
    for (const auto& testVector : testVectors)
    {
        Uri::Uri uri;

        ASSERT_TRUE(uri.ParseFromString(testVector.pathIn)) << "Failed Index: "<<index;
        ASSERT_EQ(testVector.pathOut, uri.GetPath()) << "Failed Index: " << index;
        ++index;
    }
}

TEST(UriTests, ParseFromStringHasAPortNumber) {
    Uri::Uri uri;

    ASSERT_TRUE(uri.ParseFromString("http://www.example.com:8080/foo/bar"));
    ASSERT_EQ("http", uri.GetScheme());
    ASSERT_EQ("www.example.com", uri.GetHost());
    ASSERT_TRUE(uri.HasPort());
    ASSERT_EQ(8080, uri.GetPort());
    ASSERT_EQ(
        (std::vector<std::string>
    {
        "",
            "foo",
            "bar",
    }
    ),
        uri.GetPath()
        );
}

TEST(UriTests, ParseFromStringTwiceWhenHasAPortNumber) {
    Uri::Uri uri;

    ASSERT_TRUE(uri.ParseFromString("http://www.example.com:8080/foo/bar"));
    ASSERT_TRUE(uri.ParseFromString("http://www.example.com/foo/bar"));
    ASSERT_EQ("http", uri.GetScheme());
    ASSERT_EQ("www.example.com", uri.GetHost());
    ASSERT_FALSE(uri.HasPort());
    ASSERT_EQ(0, uri.GetPort());
    ASSERT_EQ(
        (std::vector<std::string>
    {
        "",
            "foo",
            "bar",
    }
    ),
        uri.GetPath()
        );
}

TEST(UriTests, ParseFromStringNotHaveAPortNumber) {
    Uri::Uri uri;

    ASSERT_TRUE(uri.ParseFromString("http://www.example.com/foo/bar"));
    ASSERT_EQ("http", uri.GetScheme());
    ASSERT_EQ("www.example.com", uri.GetHost());
    ASSERT_FALSE(uri.HasPort());
    ASSERT_EQ(
        (std::vector<std::string>
    {
        "",
            "foo",
            "bar",
    }
    ),
        uri.GetPath()
        );
}

TEST(UriTests, ParseFromStringInvalidPortNumberPurelyAlphabetic) {
    Uri::Uri uri;

    ASSERT_FALSE(uri.ParseFromString("http://www.example.com:spam/foo/bar"));
    ASSERT_EQ("http", uri.GetScheme());
}

TEST(UriTests, ParseFromStringInvalidPortNumberStartsNumericEndsAlphabetic) {
    Uri::Uri uri;

    ASSERT_FALSE(uri.ParseFromString("http://www.example.com:8080spam/foo/bar"));
    ASSERT_EQ("http", uri.GetScheme());
}

TEST(UriTests, ParseFromStringValidPortNumberTooBig) {
    Uri::Uri uri;

    ASSERT_TRUE(uri.ParseFromString("http://www.example.com:65535/foo/bar"));
    ASSERT_EQ(65535, uri.GetPort());
    ASSERT_EQ("http", uri.GetScheme());
}

TEST(UriTests, ParseFromStringInvalidPortNumberTooBig) {
    Uri::Uri uri;

    ASSERT_FALSE(uri.ParseFromString("http://www.example.com:65536/foo/bar"));
    ASSERT_EQ("http", uri.GetScheme());
}

TEST(UriTests, ParseFromStringInvalidPortNumberNegative) {
    Uri::Uri uri;

    ASSERT_FALSE(uri.ParseFromString("http://www.example.com:-8080/foo/bar"));
    ASSERT_EQ("http", uri.GetScheme());
}

TEST(UriTests, ParseFromStringEndsAfterAuthority) {
    Uri::Uri uri;

    ASSERT_TRUE(uri.ParseFromString("http://www.example.com"));
}
TEST(UriTests, ParseFromStringIsARelativeVsNonRelativeReference) {

    struct TestVector {
        std::string pathIn;
        bool isRelativeReference;
    };

    const std::vector<TestVector> testVectors
    {
        {"http://www.example.com:65535/", false},
        {"http://www.example.com:65535", false},
        {"/", true},
        {"foo", true},
    };

    size_t index = 0;
    for (const auto& testVector : testVectors)
    {
        Uri::Uri uri;

        ASSERT_TRUE(uri.ParseFromString(testVector.pathIn)) << "Failed Index: " << index;
        ASSERT_EQ(testVector.isRelativeReference, uri.IsRelativeReference()) << "Failed Index: " << index;
        ++index;
    }
}

TEST(UriTests, ParseFromStringIsARelativeVsNonRelativePath) {

    struct TestVector {
        std::string pathIn;
        bool isRelativePath;
    };

    const std::vector<TestVector> testVectors
    {
        {"http://www.example.com:65535/", false},
        {"http://www.example.com:65535", false},
        {"/", false},
        {"foo", true},

        /*
         * This is only a valid test vector if we understand
         * correctly that an empty string IS a valid
         * "relative reference" URI with an empty path.
         */
        {"", true},
    };

    size_t index = 0;
    for (const auto& testVector : testVectors)
    {
        Uri::Uri uri;

        ASSERT_TRUE(uri.ParseFromString(testVector.pathIn)) << "Failed Index: " << index;
        ASSERT_EQ(testVector.isRelativePath, uri.ContainsRelativePath()) << "Failed Index: " << index;
        ++index;
    }
}

TEST(UriTests, ParseFromStringQueryAndFragmentElements) {
    struct TestVector {
        std::string uriString;
        std::string host;
        std::string query;
        std::string fragment;
    };

    const std::vector<TestVector> testVectors
    {
        {"http://www.example.com:65535/", "www.example.com", "", ""},

        /*
         * NOTE: curiously, but we think this is correct, that
         * having a trailing question mark is equivalent to not having
         * any question mark, because in both cases, the query element
         * is empty string.  Perhaps research deeper to see if this is right.
         */
        {"http://example.com:65535?", "example.com", "", ""},
        {"http://example.com:65535?foo", "example.com", "foo", ""},
        {"http://www.example.com:65535#foo", "www.example.com", "", "foo"},
        {"http://www.example.com:65535?foo#bar", "www.example.com", "foo", "bar"},
        {"http://www.example.com:65535?earth?day#bar", "www.example.com", "earth?day", "bar"},
        {"http://www.example.com:65535/spam?foo#bar", "www.example.com", "foo", "bar"},
        {"http://www.example.com/", "www.example.com", "", ""},
        {"http://example.com?foo", "example.com", "foo", ""},
        {"http://www.example.com#foo", "www.example.com", "", "foo"},
        {"http://www.example.com?foo#bar", "www.example.com", "foo", "bar"},
        {"http://www.example.com:65535/spam?foo#bar", "www.example.com", "foo", "bar"},
    };

    size_t index = 0;
    for (const auto& testVector : testVectors)
    {
        Uri::Uri uri;

         ASSERT_TRUE(uri.ParseFromString(testVector.uriString)) << "Failed Index: " << index;
        ASSERT_EQ(testVector.host, uri.GetHost()) << "Failed Index: " << index;
        ASSERT_EQ(testVector.query, uri.GetQuery()) << "Failed Index: " << index;
        ASSERT_EQ(testVector.fragment, uri.GetFragment()) << "Failed Index: " << index;
        ++index;
    }
}

TEST(UriTests, ParseFromStringUserInfo) {
    struct TestVector {
        std::string uriString;
        std::string userInfo;
    };
    const std::vector< TestVector > testVectors{
        {"http://www.example.com/", ""},
        {"http://joe@www.example.com", "joe"},
        {"http://pepe:feen@www.example.com", "pepe:feen"},
        {"//www.example.com", ""},
        {"//bob@www.example.com", "bob"},
        {"/", ""},
        {"foo", ""},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_TRUE(uri.ParseFromString(testVector.uriString)) << index;
        ASSERT_EQ(testVector.userInfo, uri.GetUserInfo()) << index;
        ++index;
    }
}

TEST(UriTests, ParseFromStringTwiceWhenHasUserInfoThenWithout) {
    Uri::Uri uri;

    ASSERT_TRUE(uri.ParseFromString("http://joe@www.example.com:8080/foo/bar"));
    ASSERT_TRUE(uri.ParseFromString("/foo/bar"));
    /*ASSERT_EQ("http", uri.GetScheme());
    ASSERT_EQ("www.example.com", uri.GetHost());
    ASSERT_FALSE(uri.HasPort());
    ASSERT_EQ(0, uri.GetPort());
    ASSERT_EQ(
        (std::vector<std::string>
            {
                "",
                 "foo",
                "bar",
            }
        ),
        uri.GetPath()
    );*/
    ASSERT_TRUE(uri.GetUserInfo().empty());
}

TEST(UriTests, ParseFromStringSchemeIllegalCharacters)
{
    /*
    ADDITIONAL
    ===========
    // For the practice, when do not have any expected outcome, can use a string vector
    // For the practice, if any case have different expected outcomes for different inputs, keep a struct to stor the input, output(s) and use a vector of that struct.

    */

    const std::vector< std::string > testVectors{
        {"://www.example.com/"},
        {"0://www.example.com/"},
        {"+://www.example.com/"},
        {"@://www.example.com/"},
        {".://www.example.com/"},
        {"h@://www.example.com/"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_FALSE(uri.ParseFromString(testVector)) << index;
        ////ASSERT_EQ(testVector, uri.GetUserInfo()) << index;
        ++index;
    }
}

TEST(UriTests, ParseFromStringSchemeBarelyLegal) {
    struct TestVector {
        std::string uriString;
        std::string scheme;
    };
    const std::vector< TestVector > testVectors{
        {"h://www.example.com/", "h"},
        {"x+://joe@www.example.com", "x+"},
        {"y-://joe@www.example.com", "y-"},
        {"z.://pepe:feen@www.example.com", "z."},
        {"aa://joe@www.example.com", "aa"},
        {"a0://joe@www.example.com", "a0"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_TRUE(uri.ParseFromString(testVector.uriString)) << index;
        ASSERT_EQ(testVector.scheme, uri.GetScheme()) << index;
        ++index;
    }
}

TEST(UriTests, ParseFromStringSchemeMixedCase) {
    const std::vector< std::string > testVectors{
        {"http://www.example.com/"},
        {"hTtp://www.example.com/"},
        {"HTTP://www.example.com/"},
        {"Http://www.example.com/"},
        {"HttP://www.example.com/"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_TRUE(uri.ParseFromString(testVector)) << index;
        ASSERT_EQ("http", uri.GetScheme()) << ">>> Failed for test vector element " << index << " <<<";
        ++index;
    }
}

TEST(UriTests, ParseFromStringUserInfoIllegalCharacters) {
    const std::vector< std::string > testVectors{
        {"//%X@www.example.com/"},
        {"//{@www.example.com/"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_FALSE(uri.ParseFromString(testVector)) << index;
        ++index;
    }
}

TEST(UriTests, ParseFromStringUserInfoBarelyLegal) {
    struct TestVector {
        std::string uriString;
        std::string userInfo;
    };
    const std::vector< TestVector > testVectors{
        //{"//%@www.example.com/", "%"},
        {"//%41@www.example.com/", "A"},
        {"//@www.example.com/", ""},
        {"//!@www.example.com/", "!"},
        {"//'@www.example.com/", "'"},
        {"//(@www.example.com/", "("},
        {"//;@www.example.com/", ";"},
        {"http://:@www.example.com/", ":"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_TRUE(uri.ParseFromString(testVector.uriString)) << index;
        ASSERT_EQ(testVector.userInfo, uri.GetUserInfo());
        ++index;
    }
}

TEST(UriTests, ParseFromStringHostIllegalCharacters) {
    const std::vector< std::string > testVectors{
        {"//%X@www.example.com/"},
        {"//@www:example.com/"},
        {"//[vX.:]/"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_FALSE(uri.ParseFromString(testVector)) << index;
        ++index;
    }
}

TEST(UriTests, ParseFromStringHostBarelyLegal) {
    struct TestVector {
        std::string uriString;
        std::string host;
    };
    const std::vector< TestVector > testVectors{
        {"//%41/", "a"},
        {"///", ""},
        {"//!/", "!"},
        {"//'/", "'"},
        {"//(/", "("},
        {"//;/", ";"},
        {"//1.2.3.4/", "1.2.3.4"},
        {"//[v7.:]/", "v7.:"},
        {"//[v7.aB:]/", "v7.aB:"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_TRUE(uri.ParseFromString(testVector.uriString)) << index;
        ASSERT_EQ(testVector.host, uri.GetHost());
        ++index;
    }
}
 
TEST(UriTests, ParseFromStringHostMixedCase) {
    const std::vector< std::string > testVectors{
        {"http://www.example.com/"},
        {"http://www.EXAMPLE.com/"},
        {"http://www.exAMple.com/"},
        {"http://www.example.cOM/"},
        {"http://wWw.exampLe.Com/"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_TRUE(uri.ParseFromString(testVector)) << index;
        ASSERT_EQ("www.example.com", uri.GetHost()) << ">>> Failed for test vector element " << index << " <<<";
        ++index;
    }
}

TEST(UriTests, ParseFromStringDontMisinterpretColonInOtherPlacesAsSchemeDelimiter) {
    const std::vector< std::string > testVectors{
        {"//foo:bar@www.example.com/"},
        {"//www.example.com/a:b"},
        {"//www.example.com/foo?a:b"},
        {"//www.example.com/foo#a:b"},
        {"//[v7.:]/"},
        {"/:/foo"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_TRUE(uri.ParseFromString(testVector)) << index;
        ASSERT_TRUE(uri.GetScheme().empty());
        ++index;
    }
}

TEST(UriTests, ParseFromStringPathIllegalCharacters) {
    const std::vector< std::string > testVectors{
        {"http://www.example.com/foo[bar"},
        {"http://www.example.com/]bar"},
        {"http://www.example.com/foo]"},
        {"http://www.example.com/["},
        {"http://www.example.com/abc/foo]"},
        {"http://www.example.com/abc/["},
        {"http://www.example.com/foo]/abc"},
        {"http://www.example.com/[/abc"},
        {"http://www.example.com/foo]/"},
        {"http://www.example.com/[/"},
        {"/foo[bar"},
        {"/]bar"},
        {"/foo]"},
        {"/["},
        {"/abc/foo]"},
        {"/abc/["},
        {"/foo]/abc"},
        {"/[/abc"},
        {"/foo]/"},
        {"/[/"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_FALSE(uri.ParseFromString(testVector)) << index;
        ++index;
    }
}

TEST(UriTests, ParseFromStringPathBarelyLegal) {
    struct TestVector {
        std::string uriString;
        std::vector< std::string > path;
    };
    const std::vector< TestVector > testVectors{
        {"/:/foo", {"", ":", "foo"}},
        {"bob@/foo", {"bob@", "foo"}},
        {"hello!", {"hello!"}},
        {"urn:hello,%20w%6Frld", {"hello, world"}},
        {"//example.com/foo/(bar)/", {"", "foo", "(bar)", ""}},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_TRUE(uri.ParseFromString(testVector.uriString)) << index;
        ASSERT_EQ(testVector.path, uri.GetPath());
        ++index;
    }
}

TEST(UriTests, ParseFromStringQueryIllegalCharacters) {
    const std::vector< std::string > testVectors{
        {"http://www.example.com/?foo[bar"},
        {"http://www.example.com/?]bar"},
        {"http://www.example.com/?foo]"},
        {"http://www.example.com/?["},
        {"http://www.example.com/?abc/foo]"},
        {"http://www.example.com/?abc/["},
        {"http://www.example.com/?foo]/abc"},
        {"http://www.example.com/?[/abc"},
        {"http://www.example.com/?foo]/"},
        {"http://www.example.com/?[/"},
        {"?foo[bar"},
        {"?]bar"},
        {"?foo]"},
        {"?["},
        {"?abc/foo]"},
        {"?abc/["},
        {"?foo]/abc"},
        {"?[/abc"},
        {"?foo]/"},
        {"?[/"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_FALSE(uri.ParseFromString(testVector)) << index;
        ++index;
    }
}

TEST(UriTests, ParseFromStringQueryBarelyLegal) {
    struct TestVector {
        std::string uriString;
        std::string query;
    };
    const std::vector< TestVector > testVectors{
        {"/?:/foo", ":/foo"},
        {"?bob@/foo", "bob@/foo"},
        {"?hello!", "hello!"},
        {"urn:?hello,%20w%6Frld", "hello, world"},
        {"//example.com/foo?(bar)/", "(bar)/"},
        {"http://www.example.com/?foo?bar", "foo?bar" },
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_TRUE(uri.ParseFromString(testVector.uriString)) << index;
        ASSERT_EQ(testVector.query, uri.GetQuery());
        ++index;
    }
}

TEST(UriTests, ParseFromStringFragmentIllegalCharacters) {
    const std::vector< std::string > testVectors{
        {"http://www.example.com/#foo[bar"},
        {"http://www.example.com/#]bar"},
        {"http://www.example.com/#foo]"},
        {"http://www.example.com/#["},
        {"http://www.example.com/#abc/foo]"},
        {"http://www.example.com/#abc/["},
        {"http://www.example.com/#foo]/abc"},
        {"http://www.example.com/#[/abc"},
        {"http://www.example.com/#foo]/"},
        {"http://www.example.com/#[/"},
        {"#foo[bar"},
        {"#]bar"},
        {"#foo]"},
        {"#["},
        {"#abc/foo]"},
        {"#abc/["},
        {"#foo]/abc"},
        {"#[/abc"},
        {"#foo]/"},
        {"#[/"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_FALSE(uri.ParseFromString(testVector)) << index;
        ++index;
    }
}

TEST(UriTests, ParseFromStringFragmentBarelyLegal) {
    struct TestVector {
        std::string uriString;
        std::string fragment;
    };
    const std::vector< TestVector > testVectors{
        {"/#:/foo", ":/foo"},
        {"#bob@/foo", "bob@/foo"},
        {"#hello!", "hello!"},
        {"urn:#hello,%20w%6Frld", "hello, world"},
        {"//example.com/foo#(bar)/", "(bar)/"},
        {"http://www.example.com/#foo?bar", "foo?bar" },
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_TRUE(uri.ParseFromString(testVector.uriString)) << index;
        ASSERT_EQ(testVector.fragment, uri.GetFragment());
        ++index;
    }
}

TEST(UriTests, ParseFromStringPathsWithPercentEncodedCharacters) {
    struct TestVector {
        std::string uriString;
        std::string pathFirstSegment;
    };
    const std::vector< TestVector > testVectors{
        {"%41", "A"},
        {"%4A", "J"},
        {"%4a", "J"},
        {"%bc", "\xbc"},
        {"%Bc", "\xbc"},
        {"%bC", "\xbc"},
        {"%BC", "\xbc"},
        {"%41%42%43", "ABC"},
        {"%41%4A%43%4b", "AJCK"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_TRUE(uri.ParseFromString(testVector.uriString)) << index;
        ASSERT_EQ(testVector.pathFirstSegment, uri.GetPath()[0]);
        ++index;
    }
}

TEST(UriTests, NormalizePath) {
    struct TestVector {
        std::string uriString;
        std::vector< std::string > normalizedPathSegments;
    };
    const std::vector< TestVector > testVectors{
        {"/a/b/c/./../../g", {"", "a", "g"}},
        {"mid/content=5/../6", {"mid", "6"}},
        {"http://example.com/a/../b", {"", "b"}},
        {"http://example.com/../b", {"", "b"}},
        {"http://example.com/a/../b/", {"", "b", ""}},
        {"http://example.com/a/../../b", {"", "b"}},
        {"./a/b", {"a", "b"}},
        {"..", {}},
        {"/", {""}},
        {"a/b/..", {"a", ""}},
        {"a/b/.", {"a", "b", ""}},
        {"a/b/./c", {"a", "b", "c"}},
        {"a/b/./c/", {"a", "b", "c", ""}},
        {"/a/b/..", {"", "a", ""}},
        {"/a/b/.", {"", "a", "b", ""}},
        {"/a/b/./c", {"", "a", "b", "c"}},
        {"/a/b/./c/", {"", "a", "b", "c", ""}},
        {"./a/b/..", {"a", ""}},
        {"./a/b/.", {"a", "b", ""}},
        {"./a/b/./c", {"a", "b", "c"}},
        {"./a/b/./c/", {"a", "b", "c", ""}},
        {"../a/b/..", {"a", ""}},
        {"../a/b/.", {"a", "b", ""}},
        {"../a/b/./c", {"a", "b", "c"}},
        {"../a/b/./c/", {"a", "b", "c", ""}},
        {"../a/b/../c", {"a", "c"}},
        {"../a/b/./../c/", {"a", "c", ""}},
        {"../a/b/./../c", {"a", "c"}},
        {"../a/b/./../c/", {"a", "c", ""}},
        {"../a/b/.././c/", {"a", "c", ""}},
        {"../a/b/.././c", {"a", "c"}},
        {"../a/b/.././c/", {"a", "c", ""}},
        {"/./c/d", {"", "c", "d"}},
        {"/../c/d", {"", "c", "d"}},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        ASSERT_TRUE(uri.ParseFromString(testVector.uriString)) << index;
        uri.NormalizePath();
        ASSERT_EQ(testVector.normalizedPathSegments, uri.GetPath()) << index;
        ++index;
    }
}

TEST(UriTests, ConstructNormalizeAndCompareEquivalentUris) {
    // This was inspired by section 6.2.2
    // of RFC 3986 (https://tools.ietf.org/html/rfc3986).
    Uri::Uri uri1, uri2;
    ASSERT_TRUE(uri1.ParseFromString("example://a/b/c/%7Bfoo%7D"));
    ASSERT_TRUE(uri2.ParseFromString("eXAMPLE://a/./b/../b/%63/%7bfoo%7d"));
    ASSERT_NE(uri1, uri2);
    uri2.NormalizePath();
    ASSERT_EQ(uri1, uri2);
}

TEST(UriTests, ReferenceResolution) {
    struct TestVector {
        std::string baseString;
        std::string relativeReferenceString;
        std::string targetString;
    };
    const std::vector< TestVector > testVectors{
        // These are all taken from section 5.4.1
        // of RFC 3986 (https://tools.ietf.org/html/rfc3986).
        {"http://a/b/c/d;p?q", "g:h", "g:h"},
        {"http://a/b/c/d;p?q", "g", "http://a/b/c/g"},
        {"http://a/b/c/d;p?q", "./g", "http://a/b/c/g"},
        {"http://a/b/c/d;p?q", "g/", "http://a/b/c/g/"},
        {"http://a/b/c/d;p?q", "/g", "http://a/g"},
        {"http://a/b/c/d;p?q", "//g", "http://g"},
        {"http://a/b/c/d;p?q", "?y", "http://a/b/c/d;p?y"},
        {"http://a/b/c/d;p?q", "g?y", "http://a/b/c/g?y"},
        {"http://a/b/c/d;p?q", "#s", "http://a/b/c/d;p?q#s"},
        {"http://a/b/c/d;p?q", "g#s", "http://a/b/c/g#s"},
        {"http://a/b/c/d;p?q", "g?y#s", "http://a/b/c/g?y#s"},
        {"http://a/b/c/d;p?q", ";x", "http://a/b/c/;x"},
        {"http://a/b/c/d;p?q", "g;x", "http://a/b/c/g;x"},
        {"http://a/b/c/d;p?q", "g;x?y#s", "http://a/b/c/g;x?y#s"},
        {"http://a/b/c/d;p?q", "", "http://a/b/c/d;p?q"},
        {"http://a/b/c/d;p?q", ".", "http://a/b/c/"},
        {"http://a/b/c/d;p?q", "./", "http://a/b/c/"},
        {"http://a/b/c/d;p?q", "..", "http://a/b/"},
        {"http://a/b/c/d;p?q", "../", "http://a/b/"},
        {"http://a/b/c/d;p?q", "../g", "http://a/b/g"},
        {"http://a/b/c/d;p?q", "../..", "http://a"},
        {"http://a/b/c/d;p?q", "../../", "http://a"},
        {"http://a/b/c/d;p?q", "../../g", "http://a/g"},

        //// Here are some examples of our own.
        {"http://example.com", "foo", "http://example.com/foo"},
        {"http://example.com/", "foo", "http://example.com/foo"},
        {"http://example.com", "foo/", "http://example.com/foo/"},
        {"http://example.com/", "foo/", "http://example.com/foo/"},
        {"http://example.com", "/foo", "http://example.com/foo"},
        {"http://example.com/", "/foo", "http://example.com/foo"},
        {"http://example.com", "/foo/", "http://example.com/foo/"},
        {"http://example.com/", "/foo/", "http://example.com/foo/"},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri baseUri, relativeReferenceUri, expectedTargetUri;
        ASSERT_TRUE(baseUri.ParseFromString(testVector.baseString));
        ASSERT_TRUE(relativeReferenceUri.ParseFromString(testVector.relativeReferenceString)) << index;
        ASSERT_TRUE(expectedTargetUri.ParseFromString(testVector.targetString)) << index;
        const auto actualTargetUri = baseUri.Resolve(relativeReferenceUri);
        ASSERT_EQ(expectedTargetUri, actualTargetUri) << index;
        ++index;
    }
}

/*
ADDITIONAL FACTS - PrintTo
=================
PrintTo - for a class
* this method decalration helps google test to know how to show test failures with class objects.
ex: show Uri objects in test failures
*/

TEST(UriTests, EmptyPathInUriWithAuthorityIsEquivalentToSlashOnlyPath) {
    Uri::Uri uri1, uri2;
    ASSERT_TRUE(uri1.ParseFromString("http://example.com"));
    ASSERT_TRUE(uri2.ParseFromString("http://example.com/"));
    ASSERT_EQ(uri1, uri2);
    //ASSERT_TRUE(uri1.ParseFromString("urn:"));
    //ASSERT_TRUE(uri2.ParseFromString("urn:/"));
    //ASSERT_EQ(uri1, uri2);
    ASSERT_TRUE(uri1.ParseFromString("//example.com"));
    ASSERT_TRUE(uri2.ParseFromString("//example.com/"));
    ASSERT_EQ(uri1, uri2);
}

TEST(UriTests, IPv6Address) {

    /*
    ADDITIONAL 
    ==========
    * Struct is like a class. So,
    * It has member variables
    * Here making a vector of struct instances - vector of objects
    * Vector has different test cases
    * Iterate the vector for testing all the test cases
    */
    struct TestVector {
        std::string uriString;
        std::string expectedHost;
        bool isValid;
    };
    const std::vector< TestVector > testVectors
    {
        // valid
        {"http://[::1]/", "::1", true},
        {"http://[::ffff:1.2.3.4]/", "::ffff:1.2.3.4", true},
        {"http://[2001:db8:85a3:8d3:1319:8a2e:370:7348]/", "2001:db8:85a3:8d3:1319:8a2e:370:7348", true},

        // invalid
        {"http://[::ffff:1.2.x.4]/", "", false},
        {"http://[::ffff:1.2.3.4.8]/", "", false},
        {"http://[::ffff:1.2.3]/", "", false},
        {"http://[::ffff:1.2.3.]/", "", false},
        {"http://[::ffff:1.2.3.256]/", "", false},
        {"http://[::fxff:1.2.3.4]/", "", false},
        {"http://[::ffff:1.2.3.-4]/", "", false},
        {"http://[::ffff:1.2.3. 4]/", "", false},
        {"http://[::ffff:1.2.3.4 ]/", "", false},
        {"http://[::ffff:1.2.3.4/", "", false},
        {"http://::ffff:1.2.3.4]/", "", false},
        {"http://::ffff:a.2.3.4]/", "", false},
        {"http://::ffff:1.a.3.4]/", "", false},
        {"http://[2001:db8:85a3:8d3:1319:8a2e:370:7348:0000]/", "", false},
        {"http://[2001:db8:85a3::8a2e:0:]/", "", false},
        {"http://[2001:db8:85a3::8a2e::]/", "", false},
        {"http://[]/", "", false},
        {"http://[:]/", "", false},
        {"http://[v]/", "", false},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        const bool parseResult = uri.ParseFromString(testVector.uriString);
        ASSERT_EQ(testVector.isValid, parseResult) << index;
        if (parseResult) {
            ASSERT_EQ(testVector.expectedHost, uri.GetHost()) << index;
        }
        ++index;
    }
}


TEST(UriTests, GenerateString) {
    struct TestVector {
        std::string scheme;
        std::string host;
        std::string query;
        std::string expectedUriString;
    };
    const std::vector< TestVector > testVectors{
        {"http", "www.example.com", "foobar", "http://www.example.com?foobar"},
        {"",     "example.com",     "bar",    "//example.com?bar"},
        {"",     "example.com",     "",       "//example.com"},
        {"",     "",                "bar",    "?bar"},
        {"http", "",                "bar",    "http:?bar"},
        {"http", "",                "",       "http:"},
        {"http", "::1",             "",       "http://[::1]"},
        {"http", "::1.2.3.4",       "",       "http://[::1.2.3.4]"},
        {"http", "1.2.3.4",         "",       "http://1.2.3.4"},
        {"",     "",                "",       ""},
    };
    size_t index = 0;
    for (const auto& testVector : testVectors) {
        Uri::Uri uri;
        uri.SetScheme(testVector.scheme);
        uri.SetHost(testVector.host);
        uri.SetQuery(testVector.query);
        const auto actualUriString = uri.GenerateString();
        ASSERT_EQ(testVector.expectedUriString, actualUriString) << index;
        ++index;
    }
}
