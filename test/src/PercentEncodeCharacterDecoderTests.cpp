/**
 * @file CharacterSetTests.cpp
 *
 * This module contains the unit tests of the Uri::Uri class.
 *
 */

#include <gtest/gtest.h>
#include <src/PercentEncodeCharacterDecoder.hpp>

#include <gtest/gtest.h>
#include <stddef.h>
#include <vector>

TEST(PercentEncodedCharacterDecoderTests, GoodSequences) {
    Uri::PercentEncodeCharacterDecoder pec;
    struct TestVector {
        char sequence[2];
        char expectedOutput;
    };
    const std::vector< TestVector > testVectors{
        {{'4', '1'}, 'A'},
        {{'5', 'A'}, 'Z'},
        {{'6', 'e'}, 'n'},
    };
    size_t index = 0;
    for (auto testVector : testVectors) {
        pec = Uri::PercentEncodeCharacterDecoder();
        ASSERT_FALSE(pec.Done());
        ASSERT_TRUE(pec.NextEncodedCharacter(testVector.sequence[0]));
        ASSERT_FALSE(pec.Done());
        ASSERT_TRUE(pec.NextEncodedCharacter(testVector.sequence[1]));
        ASSERT_TRUE(pec.Done());
        ASSERT_EQ(testVector.expectedOutput, pec.GetDecodedCharacter()) << index;
        ++index;
    }
}

TEST(PercentEncodedCharacterDecoderTests, BadSequences) {
    Uri::PercentEncodeCharacterDecoder pec;
    std::vector< char > testVectors{
        'G', 'g', '.', 'z', '-', ' ', 'V',
    };
    for (auto testVector : testVectors) {
        pec = Uri::PercentEncodeCharacterDecoder();
        ASSERT_FALSE(pec.Done());
        ASSERT_FALSE(pec.NextEncodedCharacter(testVector));
    }
}
